<?xml version="1.0" encoding="UTF-8"?>
<!--
#
# Copyright(c) 2020 Cubic Defense Applications
# 9333 Balboa Ave., San Diego, California, 92123, U.S.A.
# All Rights Reserved
#
# This software is the confidential and proprietary information of Cubic
# Defense Applications ("Confidential Information").  You shall not
# disclose such Confidential Information and shall use it only in
# accordance with the terms of the license agreement you entered into
# with Cubic Defense Applications.
#

$AClog: $


-->

<!-- This file holds configurable values that may be changed by the
      DCNC System Administrator. It is read as a resource
      by the com.cubic.utils.DCNCPreferences class.
-->
<dcnclistener>
    <!-- Utility configuration -->
    <config>
        <log-directory>c:\storage\dcnclistener\log</log-directory>
        <queue-directory>c:\storage\dcnclistener\queue</queue-directory>
        <db-directory>c:\storage\dcnclistener\db</db-directory>
    </config>

    <!-- Version identifiers. -->
    <version>
        <!-- If "true", the DCNC is an Interim DCNC and will present the
             Interim menu set. Otherwise the complete menu set will be
             presented.
        -->
        <is-interim>false</is-interim>
        <!-- If true the Timing Test option will be enabled. -->
        <timing-test>false</timing-test>
    </version>

    <!-- Shutdown related properties -->
    <shutdown>
        <wait_time>5000</wait_time>
    </shutdown>

    <!-- Timing Thresholds -->
    <timing>

        <!-- The max amount of time in ms to wait for all the providers that
              registered with the Shutdown Manager to report on shutdown.
        -->
        <WaitBeforeShutDown>5000</WaitBeforeShutDown>

        <!-- Time interval (seconds) between system clock adjustments -->
        <adjustSystemClockInterval>300</adjustSystemClockInterval>

        <!-- UTC - System clock delta tolerance (milliseconds) -->
        <utcTimeSystemClockDeltaLimit>50</utcTimeSystemClockDeltaLimit>

    </timing>

    <!-- Other miscellaneous and site specific data. -->

    <site>
        <!-- GMT Offset to apply to range timing. -->
        <gmt-offset>2</gmt-offset>

        <hemisphere>
            <western>true</western>
            <southern>false</southern>
        </hemisphere>

        <!-- Whether or not to perform a default time reset on messages
             received from the field. If true, then a time offset of 0 received
             from a DCI will be interpreted as the current system time. If
             false, the same value of 0 will be interpreted as the reference
             date.
        -->
        <do-time-reset>true</do-time-reset>

        <!-- The minimum number of satellites required to establish position
             confidence.
        -->
        <required-satellites>4</required-satellites>

        <!-- Configuration to support the lab MILES 2 Simulator workaround. -->
        <lab>
            <enable-default-miles-id>false</enable-default-miles-id>
            <default-miles-id>201</default-miles-id>
        </lab>

        <!-- IP Address of DCNC's Range Network -->
        <range-net-address>127.0.0.5</range-net-address>
        <!--<range-net-address>166.130.32.50</range-net-address>-->
        <!-- Memory threshold (as a percentage of the maximum). If the heap
             exceeds this size the DCNC will attempt to take corrective action.
        -->
        <heap-threshold>0.50</heap-threshold>
    </site>

    <!-- Interfaces used by the DCNC. -->
    <interface>
        <icd>data/matbat-icd-02a-1c.xml</icd>
        <icd>data/matbat-icd-03-v4c.xml</icd>
    </interface>

    <!-- Configuration for the Message Provider -->
    <events>
        <!-- Number of threads available for processing Events. -->
        <thread-count>350</thread-count>
    </events>
</dcnclistener>
