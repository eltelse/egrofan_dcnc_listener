 package dcnclistener.applicationmanager;
 
 import java.io.File;
import java.util.logging.Level;
import java.util.logging.Logger;

import dcnclistener.hla.HLAControl;
import dcnclistener.userinterface.DcncListenerUi;
 
 
 public class ApplicationManager
   implements ScenariosListener
 {
   private static class InstanceHolder
   {
     private static final ApplicationManager instance = new ApplicationManager();
   }
 
   private static Logger logger = Logger.getLogger("ApplicationManager");
   private HLAControl hlaControl = null;
   public static ApplicationManager getInstance() {
     return InstanceHolder.instance;
   }
 
   
   public void exitApplication() {
     System.exit(0);
   }
 
   public StringBuffer getVersion() {
     byte b1 = 10;
     
     StringBuffer stringBuffer = new StringBuffer();
     stringBuffer.append("Eltel DCNC 2 HLA Plugin");
    // stringBuffer.append(b1);
    // stringBuffer.append(b1);
    // stringBuffer.append(b1);
     stringBuffer.append("Java: " + System.getProperty("java.version"));
     
     stringBuffer.append(b1);
     stringBuffer.append("ClassPath:");
			System.out.println(stringBuffer.toString());
     String str = System.getProperty("java.class.path");
     String[] arrayOfString = str.split(File.pathSeparator);
     for (byte b2 = 0; b2 < arrayOfString.length; b2++) {
       
       stringBuffer.append(b1);
       stringBuffer.append(arrayOfString[b2]);
     } 
     return stringBuffer;
   }
 
   public void initialize() {
     logger.info("\n<<< Version Information Begin >>"+
         getVersion().toString() + "\n<<< Version Information End >>>");
 
     
     Runnable runnable = () -> DcncListenerUi.launch(DcncListenerUi.class, new String[0]);
     
     (new Thread(runnable)).start();
 
     
     while (!DcncListenerUi.isStagingComplete()) {
 
       
       try {
         Thread.sleep(100L);
       }
       catch (InterruptedException interruptedException) {
         
         logger.log(Level.SEVERE, "Exception Caught", interruptedException);
       } 
     } 
     
     if (this.hlaControl == null) {
       
       logger.info("Starting up HLAControl...");
       this.hlaControl = new HLAControl();
     }
     else {
       
       logger.info("HLAControl ALREADY instantiated.");
     } 
 
     
     try {
       logger.info("Waiting 3 seconds to start GPB I/F...");
       Thread.sleep(3000L);
     }
     catch (InterruptedException interruptedException) {
       
       logger.log(Level.SEVERE, "Exception Caught", interruptedException);
     } 
     
     logger.info("Starting up GPBProcessor...");
     
     DcncListenerUi.addScenariosListener(this);
     
   }
   
   public void exerciseConnectReceived() {
     logger.info("ConnectExercise event.");
     if (this.hlaControl == null)
     {
       this.hlaControl = new HLAControl();
     }
   }
 
   public void exerciseDisconnectReceived() {
     logger.info("DisconnectExercise event.");
   }

   public void completeScenarios() {
     logger.info("Complete Scenarios at the DCNC event.");
   }

   public void refreshScenarios() {
     logger.info("Refresh Scenarios event.");
   }
   
   private ApplicationManager() {}
 }
