 package dcnclistener.exercise;
 
 import com.cubic.pcris.ScenarioStatus;
 
 public class ScenarioDescriptor
 {
   private String name = "";
   
   private int identifier = 0;
 
   
   private int exconId = 0;
 
   private boolean active = false;
   
   private ScenarioStatus scenarioStatus;
 
 
 
   
   public ScenarioDescriptor() {}
 
 
 
   
   public ScenarioDescriptor(int paramInt, String paramString) {
     this.identifier = paramInt;
     this.name = paramString;
     this.active = true;
     this.scenarioStatus = ScenarioStatus.ACTIVE;
   }
 
 
 
 

   public ScenarioDescriptor(int paramInt1, int paramInt2, String paramString) {
     this.identifier = paramInt1;
     this.exconId = paramInt2;
     this.name = paramString;
     this.active = true;
     this.scenarioStatus = ScenarioStatus.ACTIVE;
   }
 

   public int getId() {
     return this.identifier;
   }
 

   public void setId(int paramInt) {
     this.identifier = paramInt;
   }
 

   public String getName() {
     return this.name;
   }
 

   public void setName(String paramString) {
     this.name = paramString;
   }
 

   public boolean isActive() {
     return this.active;
   }
 
 

   public void setActive(boolean paramBoolean) {
     this.active = paramBoolean;
   }
 

   public ScenarioStatus getStatus() {
     return this.scenarioStatus;
   }
 
   
   public void setStatus(ScenarioStatus paramScenarioStatus) {
     this.scenarioStatus = paramScenarioStatus;
   }
 
 
 

   public boolean equals(Object paramObject) {
     boolean bool = false;
     
     if (paramObject != null && paramObject instanceof ScenarioDescriptor) {
       
       ScenarioDescriptor scenarioDescriptor = (ScenarioDescriptor)paramObject;
       bool = (this.identifier == scenarioDescriptor.identifier) ? true : false;
     } 
     return bool;
   }

   public int hashCode() {
     return 29 * this.identifier;
   }

   public String toString() {
     StringBuffer stringBuffer = new StringBuffer();
     stringBuffer.append("ScenarioDescriptor: identifier=" + this.identifier + ", name=" + this.name + ", exconId=" + this.exconId);
     
     return stringBuffer.toString();
   }
 }
