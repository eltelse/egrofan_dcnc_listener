 package dcnclistener.exercise;
 
 import com.cubic.pcris.LatLon;
import com.cubic.pcris.LatLonAlt;
import com.cubic.pcris.ScenarioMode;
import com.cubic.pcris.SimulationWarningMode;
public class ExerciseData
 {
   private long exerciseID;
   private long scenarioID;
   private ScenarioMode scenarioMode;
   private SimulationWarningMode warningMode;
   private LatLonAlt defaultPosition;
   private long startTime;
   
   private boolean active = true;
   
   public ExerciseData() {
     this.exerciseID = 0L;
     this.scenarioID = 0L;
     
     this.defaultPosition = new LatLonAlt();
     
     this.scenarioMode = ScenarioMode.DAY;
     this.warningMode = SimulationWarningMode.FULL;
     this.active = true;
     
     setStartTime(System.currentTimeMillis());
   }
 
 

   public ExerciseData(long paramLong1, long paramLong2) {
     this();
     
     this.exerciseID = paramLong1;
     this.scenarioID = paramLong2;
   }

 

   public ExerciseData(long paramLong1, long paramLong2, double paramDouble1, double paramDouble2, double paramDouble3, ScenarioMode paramScenarioMode, SimulationWarningMode paramSimulationWarningMode, long paramLong3) {
     this(paramLong1, paramLong2);
     
     this.defaultPosition.setLatitude(paramDouble1);
     this.defaultPosition.setLongitude(paramDouble2);
     this.defaultPosition.setAltitude(paramDouble3);
     
     this.scenarioMode = paramScenarioMode;
     this.warningMode = paramSimulationWarningMode;
     this.startTime = paramLong3;
   }
 

   public void setExerciseID(long paramLong) {
     this.exerciseID = paramLong;
   }
 

   public long getExerciseID() {
     return this.exerciseID;
   }
 

   public void setScenarioID(long paramLong) {
     this.scenarioID = paramLong;
   }
 

   public long getScenarioID() {
     return this.scenarioID;
   }
 

   public void setLatitude(double paramDouble) {
     this.defaultPosition.setLatitude(paramDouble);
   }
 

   public double getLatitude() {
     return this.defaultPosition.getLatitude();
   }
 

   public void setLongitude(double paramDouble) {
     this.defaultPosition.setLongitude(paramDouble);
   }
 

   public double getLongitude() {
     return this.defaultPosition.getLongitude();
   }
 

   public void setAltitude(double paramDouble) {
     this.defaultPosition.setAltitude(paramDouble);
   }
 

   public double getAltitude() {
     return this.defaultPosition.getAltitude();
   }
 

   public void setPosition(LatLon paramLatLon) {
     this.defaultPosition.setLatitude(paramLatLon.getLatitude());
     this.defaultPosition.setLongitude(paramLatLon.getLongitude());
   }
 

   public LatLonAlt getPosition() {
     return new LatLonAlt(this.defaultPosition
         .getLatitude(), this.defaultPosition
         .getLongitude(), this.defaultPosition
         .getAltitude());
   }
 

   public ScenarioMode getScenarioMode() {
     return this.scenarioMode;
   }
 

   public void setScenarioMode(ScenarioMode paramScenarioMode) {
     this.scenarioMode = paramScenarioMode;
   }
 

   public SimulationWarningMode getSimulationWarningMode() {
     return this.warningMode;
   }
 

   public void setSimulationWarningMode(SimulationWarningMode paramSimulationWarningMode) {
     this.warningMode = paramSimulationWarningMode;
   }
 

   public void setStartTime(long paramLong) {
     this.startTime = paramLong;
   }
 

   public long getStartTime() {
     return this.startTime;
   }
 

   public boolean isActive() {
     return this.active;
   }
 

   public void setActive(boolean paramBoolean) {
     this.active = paramBoolean;
   }
 

   public String toString() {
     StringBuffer stringBuffer = new StringBuffer();
     stringBuffer.append(Long.toString(this.exerciseID));
     stringBuffer.append(":");
     stringBuffer.append(Long.toString(this.scenarioID));
     
     return stringBuffer.toString();
   }
 }

