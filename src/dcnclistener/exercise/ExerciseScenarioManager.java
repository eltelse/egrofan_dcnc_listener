 package dcnclistener.exercise;
 
 import com.cubic.pcris.ExerciseData;
 import com.cubic.pcris.ExerciseListInteraction;
 import com.cubic.pcris.LatLon;
 import com.cubic.pcris.ScenarioMode;
 import com.cubic.pcris.ScenarioStatus;
 import com.cubic.pcris.SimulationWarningMode;
 import java.util.ArrayList;
 import java.util.HashMap;
 import java.util.List;
 import java.util.Map;
 import java.util.logging.Logger;

 public class ExerciseScenarioManager
 {
   private static class InstanceHolder
   {
     private static final ExerciseScenarioManager instance = new ExerciseScenarioManager();
   }
 
 
   
   public static long SCENARIO_1 = 1L;
 
 
   
   private static Logger logger = Logger.getLogger(ExerciseScenarioManager.class.getName());
 
   
   private static HashMap<Long, ScenarioDescriptor> scenarioList = new HashMap<>();
 
 
   
   private ExerciseListInteraction currentExerciseList;
 
   
   private Map<Long, ExerciseData> map;
 
   
   private static Object lock = new Object();

   private ExerciseScenarioManager() {
     this.map = new HashMap<>();
   }
 
 

   public static ExerciseScenarioManager getInstance() {
     return InstanceHolder.instance;
   }
 

   public List<ScenarioDescriptor> getScenarioList() {
     return new ArrayList<>(scenarioList.values());
   }
 

   public boolean isScenarioListEmpty() {
     return scenarioList.isEmpty();
   }
 
 
 

   public boolean handleConnectExercise(int paramInt, String paramString) {
     boolean bool = scenarioList.isEmpty();
     
     if (!scenarioList.containsKey(Long.valueOf(paramInt))) {
       
       ScenarioDescriptor scenarioDescriptor = new ScenarioDescriptor(paramInt, paramString);
       
       logger.info("New Scenario Name=" + paramString + ", Id=" + paramInt + " received");
       
       scenarioList.put(Long.valueOf(paramInt), scenarioDescriptor);
     } 
     
     return bool;
   }

   public void clear() {
     scenarioList.clear();
   }
 
 

   public int getActiveScenarios() {
     byte b = 0;
     for (ScenarioDescriptor scenarioDescriptor : scenarioList.values()) {
       
       if (scenarioDescriptor.getStatus() == ScenarioStatus.ACTIVE)
       {
         b++;
       }
     } 
     return b;
   }
 
 
 

   public boolean handleDisconnectExercise(int paramInt) {
     ScenarioDescriptor scenarioDescriptor = getScenarioData(paramInt);
     if (scenarioDescriptor != null) {
       
       scenarioDescriptor.setActive(false);
       scenarioDescriptor.setStatus(ScenarioStatus.COMPLETED);
       logger.info("Scenario Id=" + paramInt + " completed");
     }
     else {
       
       logger.info("Scenario Id=" + paramInt + " NOT completed!");
     } 
     
     return scenarioList.isEmpty();
   }
 
 
 

   private ScenarioDescriptor getScenarioData(int paramInt) {
     return scenarioList.get(Long.valueOf(paramInt));
   }
 

   public boolean isActive(long paramLong) {
     boolean bool = false;
     ScenarioDescriptor scenarioDescriptor = scenarioList.get(Long.valueOf(paramLong));
     if (scenarioDescriptor != null) {
       
       bool = scenarioDescriptor.isActive();
     }
     else {
       
       logger.finest("ExerciseScenarioManager: ExerciseId=" + paramLong + " not found!");
     } 
     
     return bool;
   }
 
 
 

   public boolean exists(long paramLong) {
     synchronized (scenarioList) {
       
       return scenarioList.containsKey(Long.valueOf(paramLong));
     } 
   }
 

   public boolean isActiveExercise(long paramLong) {
     boolean bool = false;
     ExerciseData exerciseData = getData(paramLong, SCENARIO_1);
     if (exerciseData != null) {
       
    //   bool = exerciseData.isActive();
     }
     else {
       
       logger.info("ExerciseManager:   ExerciseId=" + paramLong + " not found!");
     } 
     
     return bool;
   }
 

   public void inactive(long paramLong) {
     synchronized (this.map) {
       
       this.map.remove(Long.valueOf(paramLong));
     } 
   }
 
 
 

   public void setData(long paramLong1, long paramLong2, LatLon paramLatLon, ScenarioMode paramScenarioMode, SimulationWarningMode paramSimulationWarningMode, long paramLong3) {
     ExerciseData exerciseData = null;
     synchronized (this.map) {
       
       if (this.map.containsKey(Long.valueOf(paramLong1))) {
         
         exerciseData = this.map.get(Long.valueOf(paramLong1));
       }
       else {
         
//         exerciseData = new ExerciseData(paramLong1, paramLong2);
         this.map.put(Long.valueOf(paramLong1), exerciseData);
       } 
       
//       exerciseData.setPosition(paramLatLon);
//       exerciseData.setScenarioMode(paramScenarioMode);
//       exerciseData.setSimulationWarningMode(paramSimulationWarningMode);
//       exerciseData.setStartTime(paramLong3);
     } 
   }
 
 
 

   public ExerciseData getData(long paramLong1, long paramLong2) {
     ExerciseData exerciseData = null;
     synchronized (this.map) {
       
       if (this.map.containsKey(Long.valueOf(paramLong1)))
       {
         exerciseData = this.map.get(Long.valueOf(paramLong1));
       }
     } 
     
     return exerciseData;
   }
 

   public ExerciseListInteraction getExerciseList() {
     synchronized (lock) {
       
       return this.currentExerciseList;
     } 
   }
 

   public void setExerciseList(ExerciseListInteraction paramExerciseListInteraction) {
     synchronized (lock) {
       
       this.currentExerciseList = paramExerciseListInteraction;
     } 
   }
 
 

   public boolean exerciseListChanged(ExerciseListInteraction paramExerciseListInteraction) {
     boolean bool = true;
     
     synchronized (lock) {
       
       if (this.currentExerciseList != null) {
 
         
         ExerciseData[] arrayOfExerciseData1 = paramExerciseListInteraction.getExercises();
         
         ExerciseData[] arrayOfExerciseData2 = this.currentExerciseList.getExercises();
         
         if (arrayOfExerciseData1 != null && arrayOfExerciseData2 != null)
         {
           
           bool = (arrayOfExerciseData1.length != arrayOfExerciseData2.length) ? true : false;
         }
       } 
       logger.fine("ExerciseListChanged=" + bool);
     } 
     
     return bool;
   }
 }


/*\exercise\ExerciseScenarioManager.class

 */