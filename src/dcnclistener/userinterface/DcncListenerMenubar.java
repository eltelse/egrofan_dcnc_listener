 package dcnclistener.userinterface;
 

 import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.LogManager;
import java.util.logging.Logger;

import javafx.scene.control.Menu;
import javafx.scene.control.MenuBar;
import javafx.scene.control.MenuItem;
import javafx.scene.control.RadioMenuItem;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyCodeCombination;
import javafx.scene.input.KeyCombination;

 public class DcncListenerMenubar
   extends MenuBar
 {
   private RadioMenuItem logFromFileMenuItem;
   
   public enum MenuItems
   {
     ITEM_START, ITEM_STOP;
   }

   private static Logger logger = Logger.getLogger(DcncListenerMenubar.class.getName());
 
   private static final ResourceBundle RES = ResourceBundle.getBundle("dcnclistener.userinterface.Res");
 

   protected DcncListenerMenubar(DcncListenerUi paramDcncGatewayUi) {
     Menu menu1 = new Menu(RES.getString("FileMenu"));
     
     MenuItem menuItem1 = new MenuItem(RES.getString("FileQuitItem"));
     menuItem1.setAccelerator(new KeyCodeCombination(KeyCode.Q, new KeyCombination.Modifier[] { KeyCombination.CONTROL_DOWN }));
     
     menuItem1.setOnAction(paramActionEvent -> paramDcncGatewayUi.actionQuit());
 
     menu1.getItems().addAll(new MenuItem[] { menuItem1 });
//  
     
     Menu menu2 = new Menu("Action");
     
     MenuItem menuItem2 = new MenuItem("Get Data");
     
     menuItem2.setOnAction(paramActionEvent -> paramDcncGatewayUi.actionGetData());
 
     menu2.getItems().addAll(new MenuItem[] { menuItem2 });
     
     Menu menu6 = new Menu(RES.getString("HelpMenu"));
     
     MenuItem menuItem7 = new MenuItem(RES.getString("HelpAboutItem"));
     menuItem7.setOnAction(paramActionEvent -> paramDcncGatewayUi.actionHelpAbout());
     menu6.getItems().add(menuItem7);
     getMenus().addAll(new Menu[] { menu1, menu2, menu6 });
   }

 
   protected void logReadConfiguration() {
     try {
       logger.severe("Set Logging Level: from file.");
       LogManager.getLogManager().readConfiguration();
     
     }
     catch (Exception exception) {
       
       logger.log(Level.SEVERE, "Exception Caught", exception);
     } 
     this.logFromFileMenuItem.setSelected(true);
   }
 }


