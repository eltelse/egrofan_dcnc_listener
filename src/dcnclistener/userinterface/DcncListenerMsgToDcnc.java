package dcnclistener.userinterface;

import java.util.Optional;
import java.util.ResourceBundle;
import java.util.logging.Logger;

import javafx.application.Application;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.ButtonType;
import javafx.scene.control.Label;
import javafx.scene.image.Image;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.HBox;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.stage.StageStyle;
import dcnclistener.applicationmanager.ApplicationManager;
import dcnclistener.applicationmanager.ScenariosListener;
import dcnclistener.hla.HLAControl;

import com.cubic.pcris.AmmunitionCarried;
import com.cubic.pcris.BdaCommand;
import com.cubic.pcris.BdaSource;
import com.cubic.pcris.BdaStatus;
import com.cubic.pcris.ChemicalAlertLevel;
import com.cubic.pcris.CommandPlayer;
import com.cubic.pcris.ContaminationState;
import com.cubic.pcris.DataRequestedType;
import com.cubic.pcris.EntityAssignmentDescriptor;
import com.cubic.pcris.EventFlag;
import com.cubic.pcris.ExerciseData;
import com.cubic.pcris.ExerciseInteraction;
import com.cubic.pcris.ExerciseListInteraction;
import com.cubic.pcris.ExerciseListRequestInteraction;
import com.cubic.pcris.ExerciseStatus;
import com.cubic.pcris.ExerciseStructureChangeInteraction;
import com.cubic.pcris.ExerciseStructureInteraction;
import com.cubic.pcris.ExerciseStructureRecord;
import com.cubic.pcris.ExerciseTemplateStructureInteraction;
import com.cubic.pcris.ForceData;
import com.cubic.pcris.LatLon;
import com.cubic.pcris.MATBATCommandPlayer;
import com.cubic.pcris.MATBATPlayerAdminCommandInteraction;
import com.cubic.pcris.MATBATPlayerConfigurationInformation;
import com.cubic.pcris.MATBATPlayerConfigurationInteraction;
import com.cubic.pcris.MATBATPlayerStatusInteraction;
import com.cubic.pcris.PlayerConfigurationAction;
import com.cubic.pcris.PlayerInitializationInteraction;
import com.cubic.pcris.PlayerListRequestInteraction;
import com.cubic.pcris.PlayerStructureData;
import com.cubic.pcris.PositionDisplay;
import com.cubic.pcris.ProtectionCommand;
import com.cubic.pcris.ProtectionCommands;
import com.cubic.pcris.RecoveryActions;
import com.cubic.pcris.RequestDataInteraction;
import com.cubic.pcris.ScenarioData;
import com.cubic.pcris.ScenarioMode;
import com.cubic.pcris.ScenarioStatus;
import com.cubic.pcris.ScenarioStructureAdditionInteraction;
import com.cubic.pcris.SimulationWarningMode;
import com.cubic.pcris.TargetFederate;
import com.cubic.pcris.TesData;
import com.cubic.pcris.TesTypes;
import com.cubic.pcris.UnitSize;
import com.cubic.utils.ApplicationPreferences;
import com.cubic.utils.PlatformHelper;
import com.cubic.xander.hla.HLAServer;
import com.cubic.xander.hla.pitch.datatypes.playerConfigurationAction;
import com.cubic.xander.hla.pitch.datatypes.tesTypes;
import com.cubic.xander.hla.pitch.impl.encoders.ExerciseStructureDataListEncoder;
import com.cubic.xander.hla.pitch.impl.encoders.playerConfigurationActionEncoder;

public class DcncListenerMsgToDcnc extends Application {
	private static boolean SCENARIO_SUPPORT = ApplicationPreferences
			.getBoolean("scenarioSupport", Boolean.valueOf(false));
	private static final ResourceBundle RES = ResourceBundle
			.getBundle("dcnclistener.userinterface.Res");
	private static Logger logger = Logger.getLogger(DcncListenerMsgToDcnc.class
			.getName());
	private static boolean stagingComplete = false;
	private static final String FRAME_TITLE = ApplicationPreferences.get(
			"mainFrameTitle", "DCNC Listener");
	private static Image icon;
	private DcncListenerMenubar menuBar;
	private Label stateLabel;
	private static ScenariosListener scenariosListener;

	public static boolean isStagingComplete() {
		return stagingComplete;
	}

	public static void addScenariosListener(
			ScenariosListener paramScenariosListener) {
		scenariosListener = paramScenariosListener;
	}

	public void start(Stage paramStage) throws Exception {
		paramStage.setTitle(FRAME_TITLE);
		paramStage.setX(ApplicationPreferences.getInt("mainFrameX", 0));
		paramStage.setY(ApplicationPreferences.getInt("mainFrameY", 0));

		icon = new Image("file:data/CubicLogo.png");
		paramStage.getIcons().add(icon);
		BorderPane borderPane = new BorderPane();
		Scene scene = new Scene(borderPane, 300.0D, 50.0D);

		//this.menuBar = new DcncListenerMenubar(this);
		borderPane.setTop(this.menuBar);

		HBox hBox = new HBox();

		hBox.setAlignment(Pos.CENTER);

		this.stateLabel = new Label("Running Eltel DCNC 2 HLA Plugin..");
		hBox.getChildren().add(this.stateLabel);

		borderPane.setBottom(hBox);

		paramStage.setScene(scene);
		paramStage.show();

		paramStage.setOnCloseRequest(paramWindowEvent -> {
			logger.info("Windows Close(X)");

			paramWindowEvent.consume();

			actionQuit();
		});
		stagingComplete = true;
	}

	protected void actionCompleteScenarios() {
		Alert alert = new Alert(Alert.AlertType.CONFIRMATION);
		alert.setTitle(FRAME_TITLE);
		Stage stage = (Stage) alert.getDialogPane().getScene().getWindow();
		stage.getIcons().add(icon);
		alert.setHeaderText(RES
				.getString("CompleteScenariosConfirmationPrompt"));
		alert.setContentText(null);

		ButtonType buttonType1 = new ButtonType(
				RES.getString("ConfirmationYes"));

		ButtonType buttonType2 = new ButtonType(RES.getString("ConfirmationNo"));
		alert.getButtonTypes().setAll(
				new ButtonType[] { buttonType1, buttonType2 });

		Optional<ButtonType> optional = alert.showAndWait();
		if (optional.isPresent() && optional.get() == buttonType1) {
			scenariosListener.completeScenarios();
		}
	}

	public static ProtectionCommands DEFAULT_PROTECTIONS = new ProtectionCommands(
			ProtectionCommand.NO_CHANGE, ProtectionCommand.NO_CHANGE,
			ProtectionCommand.NO_CHANGE, ProtectionCommand.NO_CHANGE,
			ProtectionCommand.NO_CHANGE, ProtectionCommand.NO_CHANGE,
			ProtectionCommand.NO_CHANGE, ProtectionCommand.NO_CHANGE);
	/*      */

	public static short CREW_NO_CHANGE = 0;
	public static short SCENARIO_1 = 6;
	public static short getExconId = 1;
	public static long entityID = 30;

	public static short getExerciseId = 6;

	public void state() {
		MATBATPlayerStatusInteraction mb = new MATBATPlayerStatusInteraction();
		mb.setBDAStatus(BdaStatus.KILL);
		mb.setExerciseId(6);
		short gf = 5;
		mb.setLives(gf);
		mb.setOwningFederateId("PCRIS-EXCON");
		mb.setPlayerId(30);
		mb.setScenarioId(5);
		mb.setTime(System.currentTimeMillis());
		mb.setSendingFederateId("DCNC");
		HLAServer.publishInteraction(mb);

	}

	public void state2() {
		MATBATPlayerAdminCommandInteraction mb = new MATBATPlayerAdminCommandInteraction();
		mb.setOwningFederateId("PCRIS-EXCON");
		mb.setSendingFederateId("TestServer");
		mb.setTargetFederateId("PCRIS-EXCON");
		mb.setTime(System.currentTimeMillis());
		mb.setRecovery(false);
		mb.setExerciseId(6);
		mb.setScenarioId(5);

		MATBATCommandPlayer matbatCommandPlayer = new MATBATCommandPlayer();
		matbatCommandPlayer.setEntityID(30);
		matbatCommandPlayer.setLives((short) -1);
		matbatCommandPlayer.setCrew((short) 0);
		matbatCommandPlayer.setEventSourceFederateID("PCRIS-EXCON");
		matbatCommandPlayer.setEventSourceID(4);
		matbatCommandPlayer.setEventBDASource(BdaSource.EXCON_COMMAND);

		CommandPlayer commandPlayer = new CommandPlayer();
		commandPlayer.setBdaStatus(BdaCommand.KILL);
		commandPlayer.setPlayerId(30);
		commandPlayer.setEventFlag(EventFlag.NONE);
		commandPlayer.setRecoveryAction(RecoveryActions.NONE);
		commandPlayer.setContaminationStatus(ContaminationState.NO_CHANGE);
		ProtectionCommands protectionCommands = new ProtectionCommands(
				ProtectionCommand.NO_CHANGE, ProtectionCommand.NO_CHANGE,
				ProtectionCommand.NO_CHANGE, ProtectionCommand.NO_CHANGE,
				ProtectionCommand.NO_CHANGE, ProtectionCommand.NO_CHANGE,
				ProtectionCommand.NO_CHANGE, ProtectionCommand.NO_CHANGE);

		commandPlayer.setProtectionStatus(protectionCommands);

		CommandPlayer[] d = { commandPlayer };
		mb.setPlayerCommands(d);

		MATBATCommandPlayer[] md = { matbatCommandPlayer };
		mb.setMATBATPlayerCommands(md);

		// mb.setExerciseId(arg0);

		HLAControl.getHLAMessageSender().sendMessage(mb);
		// HLAServer.publishInteraction(mb);

	}
	 AmmunitionCarried [] aAmmunitionCarried =new AmmunitionCarried [0];
	public void getData() {
		int dciNum =1881;
		ExerciseTemplateStructureInteraction ddsdss= new ExerciseTemplateStructureInteraction();
		ForceData mForceData =new ForceData((short)1, "BlueFor");
//		ExerciseInteraction ex = new ExerciseInteraction();
//	    ex = new ExerciseInteraction();
//	    ex.setExerciseId(dciNum);
//	    ex.setOwningFederateId("PCRIS-EXCON");
//	    ex.setSendingFederateId("TestServer");
//	    ex.setTime(1643918307780L);
//		HLAControl.getHLAMessageSender().sendMessage(ex);
//		 HLAServer.publishInteraction(ex);
		 
//		 ScenarioStructureAdditionInteraction sx = new ScenarioStructureAdditionInteraction();
//		 EntityAssignmentDescriptor aad= new EntityAssignmentDescriptor();
		 
//		 sx.setEntities(new EntityAssignmentDescriptor[0] );
//		 sx.setForceId((short)1);
//		 sx.setRecovery(false);
//		 sx.setScenarioId(dciNum);
//		 sx.setOwningFederateId("PCRIS-EXCON");
//		 sx.setSendingFederateId("TestServer");
//		 sx.setTime(1643918307780L);
//		
//		 PlayerStructureData sfl = new PlayerStructureData();
//		 sfl.setEntityDesignator("///////112");
//		 sfl.setIsUnit(false);
//			 sfl.setVulnerabilityID((short) 8260);
//			 sfl.setUnitSize(UnitSize.INDIVIDUAL );
//			 sfl.setRoleID((short) 1);
//			 sfl.setPlayerUnitID( 1881);
//			 sfl.setParentEntityID( 31);
//			 sfl.setTemplateEntityID( 24);
//			 sfl.setTesData(new TesData(TesTypes.MILES_3_ENHANCED,55));
//			 
//		PlayerStructureData[] sqq= new PlayerStructureData[1];
//		sqq[0] = sfl;
//		ExerciseStructureChangeInteraction edfc = new ExerciseStructureChangeInteraction();
//		edfc.setActionRequested(PlayerConfigurationAction.ADD);
//		edfc.setEntities(sqq);
//		edfc.setExerciseId(19);
//		edfc.setForceData(new ForceData((short)1, "BlueFor"));
//		edfc.setOwningFederateId("PCRIS-EXCON");
//		edfc.setSendingFederateId("TestServer");
//		edfc.setTime(1643918307780L);
//		 HLAControl.getHLAMessageSender().sendMessage(edfc);
//		 HLAServer.publishInteraction(edfc);
//		 
	
		PlayerStructureData p1A = new PlayerStructureData();

		p1A.setEntityID(35);
		p1A.setEntityDesignator("///////1");
		p1A.setIsUnit(true);
		p1A.setParentEntityID(0);
		p1A.setRoleID((short)0);
		p1A.setTemplateEntityID(5);
		p1A.setTesData(new TesData(TesTypes.UNKNOWN,0L));
		p1A.setUnitSize(UnitSize.PLATOON);
		p1A.setVulnerabilityID((short)0);
		
		
		PlayerStructureData p1B = new PlayerStructureData();

		p1B.setEntityID(36);
		p1B.setEntityDesignator("///////111");
		p1B.setIsUnit(false);
		p1B.setParentEntityID(35);
		p1B.setRoleID((short)1);
		p1B.setTemplateEntityID(24);
		p1B.setTesData(new TesData(TesTypes.MILES_IWS,8L));
		p1B.setUnitSize(UnitSize.INDIVIDUAL);
		p1B.setPlayerUnitID(1222);
		p1B.setVulnerabilityID((short)8260);
		
		PlayerStructureData []dapp  = new PlayerStructureData[2];
		dapp[0] = p1A;
		dapp[1] = p1B;
		 
		ExerciseStructureRecord []p11 = new ExerciseStructureRecord[1];
		 p11 [0] = new ExerciseStructureRecord(mForceData,dapp);
		
		 
		 ExerciseStructureInteraction hhjg = new ExerciseStructureInteraction();
		hhjg.setExerciseId(19);
		hhjg.setExerciseStructureData(p11);
		hhjg.setRecovery(false);
		hhjg.setScenarioID(12);
		TargetFederate[] tyuio= new TargetFederate[1];
		tyuio[0] =new TargetFederate("DCNC");
		hhjg.setTargetFederateId(tyuio);
		hhjg.setOwningFederateId("PCRIS-EXCON");
		hhjg.setSendingFederateId("TestServer");
		hhjg.setTime(1643918307780L);
		
		ScenarioData serr = new ScenarioData();
		short [] ffdc= new short [1];
		ffdc[0] = 1;
		serr.setForceIDs(ffdc);
		serr.setInitialPosition(new LatLon(122.8772222,35.08));
		serr.setScenarioId(12);
		serr.setScenarioMode(ScenarioMode.DAY);
		serr.setScenarioName("dod");
		serr.setScenarioStatus(ScenarioStatus.ACTIVE);
		serr.setSimulationWarningMode(SimulationWarningMode.FULL);
		
		
		ExerciseData exerciseData = new ExerciseData();
		exerciseData.setExerciseId(19);
		exerciseData.setExerciseName("Ex-14-19-19");
		exerciseData.setExerciseStatus(ExerciseStatus.ACTIVE);
		
		ScenarioData [] rfk = new ScenarioData[1];
		rfk [0]= serr;
		exerciseData.setScenarioData(rfk);
		
		ExerciseListInteraction eddx = new ExerciseListInteraction();
		ExerciseData [] exerciseDatal = new  ExerciseData[1];
		exerciseDatal[0] =exerciseData; 
		eddx.setExercises(exerciseDatal);
		eddx.setRecovery(false);
		eddx.setTargetFederateId(tyuio);
		eddx.setOwningFederateId("PCRIS-EXCON");
		eddx.setSendingFederateId("TestServer");
		eddx.setTime(1643918307780L);
		

		HLAControl.getHLAMessageSender().sendMessage(eddx);
		 HLAServer.publishInteraction(eddx);
		HLAControl.getHLAMessageSender().sendMessage(hhjg);
		 HLAServer.publishInteraction(hhjg);
		 
		 
		 
//		MATBATPlayerConfigurationInteraction pl = new MATBATPlayerConfigurationInteraction();
//		pl.setActionRequested(PlayerConfigurationAction.ADD);
//		pl.setExerciseId(1881);
//		pl.setRecovery(false);
//		pl.setScenarioId(dciNum);
//		pl.setOwningFederateId("PCRIS-EXCON");
//		pl.setSendingFederateId("TestServer");
//		pl.setTime(1643918307780L);
		
		
//		MATBATPlayerConfigurationInformation matbatPlayerConfigurationInformation = new MATBATPlayerConfigurationInformation();
//		matbatPlayerConfigurationInformation.setAFSDEnable(true);
//		matbatPlayerConfigurationInformation.setAutoAmmoMode(true);
//		matbatPlayerConfigurationInformation.setDCUEnable(false);
//		matbatPlayerConfigurationInformation.setLiveFireMode(true);
//		matbatPlayerConfigurationInformation.setPositionDisplay(PositionDisplay.DISABLE);
//		matbatPlayerConfigurationInformation.setResurrectionTime((short)120);
//		MATBATPlayerConfigurationInformation arr [] = new MATBATPlayerConfigurationInformation[1];
//		arr[0] = matbatPlayerConfigurationInformation;
//		pl.setMATBATPlayers(arr);
//		
//		HLAControl.getHLAMessageSender().sendMessage(pl);
//		 HLAServer.publishInteraction(pl);

		
		
		PlayerInitializationInteraction player =new PlayerInitializationInteraction();
		player.setAFSDEnable(true);
		player.setAutoAmmoMode(true);
		player.setBdaStatus(BdaCommand.NOMINAL);
		player.setChemicalAlertLevel(ChemicalAlertLevel.NONE);
		player.setContaminationLevel((short)0);
		player.setCrew((short)1);
		player.setCurrentAmmunitions(aAmmunitionCarried);
		player.setDCUEnable(true);
		player.setEntityId(36);
		player.setESDActivation((short)0);
		player.setExerciseId(19);
		player.setFallbackAmmunitions(aAmmunitionCarried);
		player.setFallbackCrew((short)1);
		player.setFallbackLives((short)5);
		player.setForceId((short)1);
		player.setHitsToKill((short)1);
		player.setInvulnerable(false);
		player.setLiveFireMode(false);
		player.setLives((short)5);
		player.setOwningFederateId("PCRIS-EXCON");
		player.setPlayerDesignator("///////"+dciNum);
		player.setPlayerRole((short)1);
		player.setPlayerUnitId((short) 1222);
		player.setPositionDisplay(PositionDisplay.DISABLE);
		player.setRecovery(false);
		player.setReportOnDistance((short)2);
		player.setRequestedReportingRate(30L);
		player.setResurrectionTime((short)120);
		player.setScenarioId(12);
		player.setSendingFederateId("TestServer");
		player.setTesData(new TesData[0]);
		player.setTime(1643918307780L);
		player.setVulnerabilityType((short)8260);
		player.setWeaponId(new int [0]);
		HLAControl.getHLAMessageSender().sendMessage(player);
		 HLAServer.publishInteraction(player);
	//	PlayerListRequestInteraction playerListRequestInteraction = new PlayerListRequestInteraction();
		//playerListRequestInteraction
	//	ExerciseListRequestInteraction exerciseListRequestInteraction = new ExerciseListRequestInteraction();
//	//	exerciseListRequestInteraction.
//		RequestDataInteraction requestDataInteraction = new RequestDataInteraction();
//		requestDataInteraction.setTargetFederateID("DCNC");
//		requestDataInteraction.setSendingFederateId(HLAServer.getFederateName());
//		requestDataInteraction.setScenarioID(5);
//		requestDataInteraction.setExerciseId(6);
//		
//		DataRequestedType dataRequestedType = DataRequestedType.ALL;
//		requestDataInteraction.setDataRequested(dataRequestedType);
//		requestDataInteraction.setRecovery(false);
		//playerListRequestInteraction.
	//	mb.setMATBATPlayerCommands(md);

		// mb.setExerciseId(arg0);
//		HLAControl.getHLAMessageSender().sendMessage(requestDataInteraction);
//		HLAControl.getHLAMessageSender().sendMessage(player);
//		 HLAServer.publishInteraction(player);

	}

	protected void actionRefreshScenarios() {
		System.out.println("ionRefreshSce");
		MATBATPlayerStatusInteraction matbatPlayerStatusInteraction = new MATBATPlayerStatusInteraction();
		matbatPlayerStatusInteraction.setPlayerUnitId((short) 52);
		matbatPlayerStatusInteraction.setBDAStatus(BdaStatus.NOMINAL);
		matbatPlayerStatusInteraction.setTime(System.currentTimeMillis());
		// HLAServer.publishInteraction(matbatPlayerStatusInteraction);
		System.out
				.println("HLAServer.publishInteraction(matbatPlayerStatusInteraction);");
		state2();
	}

	protected void actionHelpAbout() {
		StringBuffer stringBuffer = ApplicationManager.getInstance()
				.getVersion();

		PlatformHelper.run(() -> {
			Alert alert = new Alert(Alert.AlertType.INFORMATION);
			alert.setTitle(FRAME_TITLE + " Help->About");
			alert.setHeaderText(null);
			alert.setContentText(stringBuffer.toString());
			alert.initModality(Modality.NONE);
			alert.initStyle(StageStyle.UTILITY);
			alert.show();
		});
	}

	protected void actionGetData() {
		System.out.println("Get Data");
		getData();
		
		
	}
	protected void actionQuit() {
		Alert alert = new Alert(Alert.AlertType.CONFIRMATION);
		alert.setTitle(FRAME_TITLE);
		Stage stage = (Stage) alert.getDialogPane().getScene().getWindow();
		stage.getIcons().add(icon);
		alert.setHeaderText(RES.getString("QuitConfirmationPrompt"));
		alert.setContentText(null);

		ButtonType buttonType1 = new ButtonType(
				RES.getString("QuitConfirmationYes"));

		ButtonType buttonType2 = new ButtonType(
				RES.getString("QuitConfirmationNo"));
		alert.getButtonTypes().setAll(
				new ButtonType[] { buttonType1, buttonType2 });

		Optional<ButtonType> optional = alert.showAndWait();
		if (optional.isPresent() && optional.get() == buttonType1) {
			ApplicationManager.getInstance().exitApplication();
		}
	}

	protected void actionViewExercise() {

		logger.info("No Exercise");
		PlatformHelper.run(() -> {
			Alert alert = new Alert(Alert.AlertType.INFORMATION);

			alert.setTitle(FRAME_TITLE + " Connected Exercise");

			alert.setHeaderText(null);

			alert.setContentText("No connected exercise.");

			alert.initModality(Modality.NONE);

			alert.initStyle(StageStyle.UTILITY);
			alert.show();
		});
	}

	protected void actionViewForces() {
		if (SCENARIO_SUPPORT) {

			actionViewForcesWithinScenarios();

		} else {

		}
	}

	protected void actionViewForcesWithinScenarios() {
		logger.info("");
		StringBuffer stringBuffer = new StringBuffer();

		PlatformHelper.run(() -> {
			Alert alert = new Alert(Alert.AlertType.INFORMATION);
			alert.setTitle(FRAME_TITLE + " Forces");
			alert.setHeaderText(null);
			alert.setContentText(stringBuffer.toString());
			alert.initModality(Modality.NONE);
			alert.initStyle(StageStyle.UTILITY);
			alert.show();
		});
	}
}

