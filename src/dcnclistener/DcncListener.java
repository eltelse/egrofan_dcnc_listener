 package dcnclistener;
 
 import dcnclistener.applicationmanager.ApplicationManager;

import com.cubic.util.ResourceManager;
 //import com.cubic.util.ResourceNotFoundException;

 import com.cubic.xander.db.ObjectDatabaseManager;
import com.cubic.xander.hla.HLAServer;
import com.cubic.xander.log.LogFileManager;
import com.cubic.xander.tx.MessageQueueManager;

import java.util.ResourceBundle;
import java.util.logging.Logger;
 
 public class DcncListener
 {
   private static final ResourceBundle RES = ResourceBundle.getBundle("dcnclistener.Res");
 
   
   private static Logger logger;
 
   
   private static DcncListener dcncListener = null;
 
   
   private ApplicationManager applicationManager = null;
 
   private DcncListener() {
     logger = Logger.getLogger(DcncListener.class.getName());
    logger.info("??");
     System.err.println("Redirejjcted stderr?");
     System.out.println("Redirected stdout?");
     this.applicationManager = ApplicationManager.getInstance();
     this.applicationManager.initialize();

   }
 
 
   public static DcncListener getInstance() {
     if (dcncListener == null)
     {
       dcncListener = new DcncListener();
     }
     return dcncListener;
   }
public static void main(String[] paramArrayOfString) {
     ResourceManager.getInstance();

 
     
     try {
       boolean bool = false;
       for (byte b = 0; b < paramArrayOfString.length; b++) {
         
         if (paramArrayOfString[b].equals("-res")) {
           
           b++;
           System.out.println(paramArrayOfString[b]);
           ResourceManager.setResourceLocation(paramArrayOfString[b]);
           bool = true;
         } 
       } 
       
       if (!bool);
 
     }
     catch (Exception exception) {
       
       error(exception);
     } 
 
     
     try {
       LogFileManager.getInstance(ResourceManager.getResourceText(RES
             .getString("ResourceID"), RES.getString("Res_LogDir")));
       MessageQueueManager.getInstance(ResourceManager.getResourceText(RES
             .getString("ResourceID"), RES.getString("Res_QueueDir")));
       ObjectDatabaseManager.getInstance(ResourceManager.getResourceText(RES
             .getString("ResourceID"), RES.getString("Res_DBDir")));
       	System.out.println("hihgi");
       HLAServer.getInstance();
       if (!HLAServer.isServerReady())
       {
         error(RES.getString("Ex_HLAError"));
       }
       HLAServer.forceReception(true);
     }
     catch (Exception resourceNotFoundException) {
       
       error((Throwable)resourceNotFoundException);
     } 
 
     
     dcncListener = getInstance();
   }

//   private static void usage() {
//     System.out.println("Usage: DCNCGateway");
//     System.out.println("\t-res <resource directory>");
//     
//     System.exit(-1);
//   }
 

   private static void error(Throwable paramThrowable) {
     paramThrowable.printStackTrace();
     System.exit(-2);
   }
 

   private static void error(String paramString) {
     System.out.println(paramString);
     System.exit(-2);
   }
 }

