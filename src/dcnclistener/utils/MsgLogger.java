 package dcnclistener.utils;
 
 import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.PrintStream;
import java.util.concurrent.LinkedBlockingQueue;

import com.cubic.pcris.BdaStatus;
import com.cubic.utils.ApplicationPreferences;
public class MsgLogger
 {
   public static final String LINE_SEP = System.getProperty("line.separator");
   public static final String SPACE = " ";
   
    private final boolean logTextMessages = Boolean.valueOf(ApplicationPreferences.get("Logging.textMessages", "false"))
     .booleanValue();
   
   private static String logDirectory = ApplicationPreferences.get("Logging.logDirectory", "log");
   
   private String logDirPath = System.getProperty("user.dir") + 
     System.getProperty("file.separator") + logDirectory;
   
   private PrintStream messagePrintStream = null;
   
   private static MsgLogger instance = null;
 
   private LinkedBlockingQueue<LogMessageEvent> logEventQueue = new LinkedBlockingQueue<>();
 
   Thread logFileWriterThread;
 
   
   private MsgLogger() {
     if (instance == null) {
       
       instance = this;
    
       try {
         File file = new File(this.logDirPath);
         if (!file.exists()) {
           
           System.out.println(this.logDirPath + " not found, creating...");
           file.mkdirs();
         } 
         createLogFiles();

      
         this.logFileWriterThread = new Thread(new LogFileWriterTask());
         this.logFileWriterThread.start();
       }
       catch (Exception exception) {
         
         exception.printStackTrace();
       } 
     } 
   }


   private void createLogFiles() {
     String str = DateTimeGroup.getLocalDTG();
 
     try {
       if (this.logTextMessages) {
         
         this.messagePrintStream = new PrintStream(new FileOutputStream(new File("log/uplink." + str + ".csv")));
         writeTextPuMessageFileHeader(this.messagePrintStream);
       } 
     
     }
     catch (FileNotFoundException fileNotFoundException) {
       
       fileNotFoundException.printStackTrace();
     } 
   }


   public static MsgLogger getInstance() {
     if (instance == null)
     {
       synchronized (MsgLogger.class) {
         
         if (instance == null)
         {
           instance = new MsgLogger();
         }
       } 
     }
     
     return instance;
   }
 
   public void logUplinkMessage(int paramInt1, int paramInt2, int paramInt3, BdaStatus paramBdaStatus, long paramLong) {
     if (this.logTextMessages) {
       LogUplinkMessage logUplinkMessage = new LogUplinkMessage(paramInt2, paramInt3, paramBdaStatus, paramLong, paramInt1);
       this.logEventQueue.offer(new LogMessageEvent(logUplinkMessage));
     } 
   }
 
   private void writeTextPuMessageFileHeader(PrintStream paramPrintStream) {
     if (paramPrintStream != null) {
       
       paramPrintStream.println("Src,Entity,DCI,FOM_Msg,GPB_Msg,DCNC_BDA,CATS_BDA,Evt_UTC,Evt_DTG");
       paramPrintStream.flush();
     } 
   }
 

   public void logCommStatusSummary(int[][] paramArrayOfint) {}
 

   private void handleLogEvent(LogMessageEvent paramLogMessageEvent) {
   }
 

   public class LogEvent
   {
     public LogEvent() {
     }
   }

 

   public class LogMessageEvent
     extends LogEvent
   {
     Object logMessage;
  
     public LogMessageEvent(Object param1Object) {
       super();
       this.logMessage = param1Object;
     }
   }

   public class LogUplinkMessage
   {
     int entityId;
     int dciId;
     BdaStatus bda;
     long time;
     int fomMsgId;
 
     
     public LogUplinkMessage(int param1Int1, int param1Int2, BdaStatus param1BdaStatus, long param1Long, int param1Int3) {
       this.entityId = param1Int1;
       this.dciId = param1Int2;
       this.bda = param1BdaStatus;
       this.time = param1Long;
       this.fomMsgId = param1Int3;
     }
   }

   public class LogFileWriterTask
     implements Runnable
   {
     public void run() {
       System.out.println("LogFileWriterTask running...");
       
       while (true) {
         try {
           while (true) {
             MsgLogger.LogMessageEvent logMessageEvent = MsgLogger.this.logEventQueue.take();
             MsgLogger.this.handleLogEvent(logMessageEvent);
           }  //break;
         } catch (Exception exception) {
           
           exception.printStackTrace();
         } 
       } 
     }
   }
 }
