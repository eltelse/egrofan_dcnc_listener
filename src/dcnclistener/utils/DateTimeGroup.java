 package dcnclistener.utils;
 
 import java.text.FieldPosition;
import java.text.MessageFormat;
import java.text.ParsePosition;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.ResourceBundle;
import java.util.TimeZone;
 
 
 
 public class DateTimeGroup
 {
   private static final ResourceBundle RES = ResourceBundle.getBundle("dcnclistener.utils.Res");
 
 
//
//   private static final long DAYLIGHT_SAVINGS_CORRECTION = 3600000L;
// 
//
//   private static final long GPSCONVERSIONCONSTANT = 315964800750L;
// 
//
//   private static final long AWESCONVERSIONCONSTANT = 315532800000L;
// 

   private static final SimpleDateFormat FORMATTER = new SimpleDateFormat("yyyyMMdd HHmmss");
 

//   private static final long MAX_GPS_SLACK_TIME = 3000L;
// 
//
//   private static final long MILLISECONDSPERSECOND = 1000L;

   private static final char[] TIMEZONECHARS = new char[] { 'Z', 'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'K', 'L', 'M', 'Y', 'X', 'W', 'V', 'U', 'T', 'S', 'R', 'Q', 'P', 'O', 'N' };

//   private static final int MSTOHOURS = 3600000;

   private static final TimeZone TIMEZONE = TimeZone.getDefault();
 
 
 
   
   private static int TIMEZONEINDEX;
 
 
 
   
   private static long utcTime = 0L;
 
 
 
   
   private static long systemTimeAtUtcSet = 0L;
 
 
 
   
   private static boolean isSystemTime = true;
 
 
   
//   private static final String LOCK_STRING = "Time Lock String";
// 
// 
//   
//   private static long lastClockUpdateTime = 0L;
//
//   private static final String dateTimeFormat = "{0,date} {0,time,HH:mm:ss.SSS}";
// 
// 
// 
   
   private static MessageFormat dateTimeFormatter = null;
 
 
 
   
   private static Object[] args = new Object[1];
 

   static {
     TIMEZONEINDEX = TIMEZONE.getRawOffset() / 3600000;
     if (TIMEZONEINDEX < 0)
     {
       TIMEZONEINDEX += 25;
     }
     
     dateTimeFormatter = new MessageFormat("{0,date} {0,time,HH:mm:ss.SSS}");
   }
 
 
 
 

   public static synchronized long getUtcTime() {
     return getUtcTimeMillis() / 1000L;
   }
 
 
 

   public static synchronized long getUtcTimeMillis() {
     long l;
     synchronized ("Time Lock String") {
 
       
       if (isSystemTime) {
         
         l = System.currentTimeMillis();
 
 
 
       
       }
       else {
 
 
 
         
         l = utcTime * 1000L + System.currentTimeMillis() - systemTimeAtUtcSet;
      
         if (System.currentTimeMillis() - systemTimeAtUtcSet > 3000L)
         {
           
           isSystemTime = true;
         }
       } 
     } 
     
     return l;
   }
 

   public static synchronized void setUtcTime(long paramLong) {
     synchronized ("Time Lock String") {
       
       isSystemTime = false;
       
       utcTime = paramLong;
       
       systemTimeAtUtcSet = System.currentTimeMillis();
     } 
   }
 
 

   public static boolean isSystemTime() {
     return isSystemTime;
   }


   public static String getDateTimeStringMillis() {
     StringBuffer stringBuffer = new StringBuffer();
     args[0] = Long.valueOf(getUtcTimeMillis());
     dateTimeFormatter.format(args, stringBuffer, (FieldPosition)null);
     return stringBuffer.toString();
   }
 
 
 

   public static synchronized Date getDateTime() {
     return new Date(getUtcTimeMillis());
   }
 
 

   public static String getLocalDTG() {
     Date date = getDateTime();
     
     String str = FORMATTER.format(date);
     StringBuffer stringBuffer = new StringBuffer(str);
 
     
     stringBuffer.setCharAt(8, TIMEZONECHARS[TIMEZONEINDEX]);
     return stringBuffer.toString();
   }


   public static String getLocalDTG(long paramLong) {
     Date date = new Date(paramLong);
     
     String str = FORMATTER.format(date);
     StringBuffer stringBuffer = new StringBuffer(str);
 
     
     stringBuffer.setCharAt(8, TIMEZONECHARS[TIMEZONEINDEX]);
     return stringBuffer.toString();
   }


   public static long convertDtgToMilliseconds(String paramString) {
     long l = 0L;
     
     if (paramString.length() > 8) {
       
       try {
         
         StringBuffer stringBuffer = new StringBuffer(paramString);
         stringBuffer.setCharAt(8, ' ');
         Date date = FORMATTER.parse(stringBuffer.toString(), new ParsePosition(0));
         
         l = date.getTime();
       }
       catch (Exception exception) {
 
         
         l = 0L;
       } 
     }
     return l;
   }
 
 
 

   public static String getGMTDTG() {
     Date date1 = getDateTime();
 
     
     long l = TIMEZONE.getRawOffset() + 3600000L;
     
     Date date2 = new Date(date1.getTime() - l);
     
     String str = FORMATTER.format(date2);
 
     
     StringBuffer stringBuffer = new StringBuffer(str);
     stringBuffer.setCharAt(8, 'Z');
     
     return stringBuffer.toString();
   }


   public static long convertGMTToLocal(long paramLong) {
     long l = TIMEZONE.getRawOffset() + 3600000L;
     
     return paramLong + l;
   }

 

   public static long getGPSTime() {
     Date date = getDateTime();
     return (date.getTime() - 315964800750L) / 1000L;
   }
 
 
 

   public static Date convertGPSToSystemTime(long paramLong) {
     paramLong *= 1000L;
     
     return new Date(paramLong + 315964800750L);
   }

   public static long convertSystemTimeToGPS(Date paramDate) {
     long l = 0L;
     if (paramDate != null) {
 
       
       l = (paramDate.getTime() - 315964800750L) / 1000L;
     
     }
     else {
       
       throw new NullPointerException(RES
           .getString("Need_Valid_Date"));
     } 
     return l;
   }
 
 
 
 

   public static Date convertAWESTimeToSystemTime(long paramLong) {
     paramLong *= 1000L;
     
     return new Date(paramLong + 315532800000L);
   }
 

   public static long convertAWESTimeToSystemTimeAsLong(long paramLong) {
     return paramLong + 315532800L;
   }

 

   public static long convertSystemTimeToAWESTime(Date paramDate) {
     long l = 0L;
     if (paramDate != null) {
 
       
       l = (paramDate.getTime() - 315532800000L) / 1000L;
     
     }
     else {
       
       throw new NullPointerException(RES
           .getString("Need_Valid_Date"));
     } 
     return l;
   }
 }
