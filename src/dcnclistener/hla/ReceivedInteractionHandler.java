 package dcnclistener.hla;
 
 import java.util.ResourceBundle;
import java.util.logging.Logger;

import dcnclistener.exercise.ExerciseScenarioManager;

import com.cubic.pcris.BaseInteraction;
import com.cubic.util.LogFileWriter;
import com.cubic.xander.hla.PCRISListener;
 public abstract class ReceivedInteractionHandler
   implements PCRISListener
 {
   public static final String LINE_SEP = System.getProperty("line.separator");
 
 
   
   public static final String SPACE = " ";
 
   
   protected static Logger logger = Logger.getLogger(ReceivedInteractionHandler.class.getName());
 
 
   
   protected static final ResourceBundle RES = ResourceBundle.getBundle("dcnclistener.hla.Res");
 
 
 
   
   protected LogFileWriter log;
 
 
 
   
   public ReceivedInteractionHandler(LogFileWriter paramLogFileWriter) {
     this.log = paramLogFileWriter;
//     this.gpbSender = GPBProcessor.getInstance().getGPBMessageSender();
   }

   public abstract void receivedInteraction(BaseInteraction paramBaseInteraction);

   protected void mark() {
     RecoveryManager.mark();
   }
 
 
 

   protected boolean shouldProcess(BaseInteraction paramBaseInteraction) {
     return (!paramBaseInteraction.getRecovery() || 
       RecoveryManager.isCurrentRequest());
   }
 
 

   protected void logIt(BaseInteraction paramBaseInteraction) {
     logger.fine("DCNCListnenr Rx FOM interaction");
     logger.fine(paramBaseInteraction.paramString());
 
     
     this.log.log(paramBaseInteraction.paramString());
   }
 
 
 

   protected void logWarning(String paramString) {}
 
 
 

   protected void logException(String paramString, Exception paramException) {
     logger.warning(paramString + LINE_SEP + paramException.getMessage());
     this.log.log(paramException);
   }

   protected boolean exerciseScenarioIsActive(long paramLong1, long paramLong2) {
     return ExerciseScenarioManager.getInstance().isActive(paramLong1);
   }
 }
