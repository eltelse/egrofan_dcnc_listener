 package dcnclistener.hla;
 
  import java.util.ResourceBundle;
import java.util.Timer;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.logging.Logger;

import redis.clients.jedis.Jedis;

import com.cubic.pcris.BaseInteraction;
import com.cubic.pcris.BdaStatus;
import com.cubic.pcris.MATBATPlayerStatusInteraction;
import com.cubic.util.LogException;
import com.cubic.util.LogFileWriter;
import com.cubic.util.ResourceManager;
import com.cubic.util.ResourceNotFoundException;
import com.cubic.xander.hla.HLAInteractionType;
import com.cubic.xander.hla.HLAServer;
import com.cubic.xander.log.LogFileManager;

import dcnclistener.exercise.ExerciseScenarioManager;



 public class HLAControl
 {
			Jedis jedis = new Jedis();
   private static final ResourceBundle RES = ResourceBundle.getBundle("dcnclistener.hla.Res");
 
   
   public static final String LINE_SEP = System.getProperty("line.separator");
 
 
   
   public static final String SPACE = " ";
 
   
   protected static Logger logger = Logger.getLogger(HLAControl.class.getName());
 
   private static LogFileWriter log;
   
   private static Object interactionLock;
   
   private static String exconFederateID;

   private RecoveryManager recoveryManager;
 
   
   private Timer recoveryTimer;
 
   
   private long recoveryPeriod;
 
   
   private static HLASender hlaSender;
 
   
   private static ExerciseScenarioManager exerciseScenarioManager;
   
   private OutputProcessor outputProcessor = null;
 
 
 
   
   private LinkedBlockingQueue<BaseInteraction> outputQueue = null;

   public HLAControl() {
		System.out.println("hlacontrol");
     interactionLock = new Object();
 
     
     try {
       log = LogFileManager.getLog(RES.getString("LogID"));
     }
     catch (LogException logException) {
       
       throw new InternalError(logException.getMessage());
     } 
 
 
     
     try {
       exconFederateID = ResourceManager.getResourceText(RES
           .getString("ResourceID"), RES
           .getString("Res_ExconFedID"));
       
       this
         .recoveryPeriod = ResourceManager.getResourceLong(RES
           .getString("ResourceID"), RES
           .getString("Res_Recovery"));
     
     }
     catch (ResourceNotFoundException resourceNotFoundException) {
       
       throw new InternalError(resourceNotFoundException.getMessage());
     } 
 
     
     if (!HLAServer.isServerReady()) {
 
 
       System.out.println("!HLAServer.isServerReady()");
       log.log(RES.getString("Log_NoHLAServer"));
     }
     else {
       System.out.println("HLAServer.isServerReady()");
       logger.info(LINE_SEP + "HLAServer is ready.");
     } 
     
     this.recoveryManager = new RecoveryManager(log);
     this.recoveryTimer = new Timer();
     this.recoveryTimer.scheduleAtFixedRate(this.recoveryManager, this.recoveryPeriod, this.recoveryPeriod);
 
     
     this.outputQueue = new LinkedBlockingQueue<>();
     this.outputProcessor = new OutputProcessor();
     this.outputProcessor.start();
     
     hlaSender = new HLASender();
     
     if (exerciseScenarioManager == null)
     {
       exerciseScenarioManager = ExerciseScenarioManager.getInstance();
     }
     
     registerInteractionListeners();
			MATBATPlayerStatusInteraction matbatPlayerStatusInteraction = new MATBATPlayerStatusInteraction();
				matbatPlayerStatusInteraction.setPlayerUnitId((short) 1024);
				matbatPlayerStatusInteraction.setBDAStatus(BdaStatus.NOMINAL);
				matbatPlayerStatusInteraction.setTime(System.currentTimeMillis());
			 HLAServer.publishInteraction(matbatPlayerStatusInteraction);
			 System.out.println("HLAServer.publishInteraction(matbatPlayerStatusInteraction);");
//			 hlaSender.sendMessage(matbatPlayerStatusInteraction);
      System.out.println("laSender.sendMessage(matbatPlayerStatusInteraction);");
     log.log(RES.getString("Log_Ready"));
     
     logger.info("HLAControl start-up complete at t=" + 
         System.currentTimeMillis());
   }
 

   String getFederateID() {
     return HLAServer.getFederateName();
   }
 

   public static Object getInteractionLock() {
     return interactionLock;
   }
 

   public static HLAMessageSender getHLAMessageSender() {
     return hlaSender;
   }
 
 

   static void publishInteraction(BaseInteraction paramBaseInteraction) {
     if (paramBaseInteraction != null && HLAServer.isServerReady()) {
       
       paramBaseInteraction.setRecovery(false);
       switch (HLAInteractionType.parse(paramBaseInteraction).value()) {
 
 
         
         case 1:
         case 4:
         case 7:
         case 15:
         case 16:
         case 17:
         case 25:
         case 86:
         case 90:
         case 95:
         case 96:
         case 111:
           paramBaseInteraction.setOwningFederateId(exconFederateID);
           break;
         
         case 2:
           paramBaseInteraction.setOwningFederateId(exconFederateID);
           break;
 
 
 
         
         default:
           paramBaseInteraction.setOwningFederateId(exconFederateID);
           break;
       } 
 
       
       HLAServer.publishInteraction(paramBaseInteraction);
       logger.fine(LINE_SEP + "HLAControl.publishInteraction:" + LINE_SEP + paramBaseInteraction
           .paramString());
     
     }
     else if (paramBaseInteraction != null) {
       
       logger.severe(LINE_SEP + "HLAServer NOT ready, FOM interaction discarded" + LINE_SEP + paramBaseInteraction
           
           .paramString());
     } 
   }

   private void registerInteractionListeners() {
     if (HLAServer.isServerReady()) {
 
       System.out.println("adding listeners");
//				HLAServer.addPCRISListener(new MATBATPLAYER
//       HLAServer.addPCRISListener(new ExerciseListRequestHandler(log, exerciseScenarioManager), HLAInteractionType.EXERCISE_LIST_REQUEST);
 
       
//       HLAServer.addPCRISListener(new ExerciseStructureRequestHandler(log, exerciseScenarioManager), HLAInteractionType.EXERCISE_STRUCTURE_REQUEST);
 
       HLAServer.addPCRISListener(new MATBATPlayerStatusHandler(log), HLAInteractionType.MATBAT_PLAYER_STATUS);
       
 
       		System.out.println("krok");
			
			jedis.set("events/city/rome", "32,15,223,828");
			jedis.set("nuir", "32,15,223,828");
			String cachedResponse = jedis.get("events/city/rome");
			jedis.publish("nuir", "  HLAServer.addPCRISListener(new MATBATPlayerStatusHandler(log), HLAInteractionType.MATBAT_PLAYER_STATUS);");
			System.out.println(cachedResponse);
       HLAServer.addPCRISListener(new MATBATRangeConfirmationHandler(log), HLAInteractionType.MATBAT_RANGE_CONFIRMATION);
//		HLAServer.addPCRISListener(new DataHandler(log), HLAInteractionType.EXERCISE_LIST);
 //
		HLAServer.addPCRISListener(new HandShakeHandler(log), HLAInteractionType.HANDSHAKE);
       
    //   HLAServer.addPCRISListener(new PlayerAmmunitionHandler(log), HLAInteractionType.PLAYER_AMMUNITION_STATUS);
 
       
//       HLAServer.addPCRISListener(new PlayerAWEHandler(log), HLAInteractionType.PLAYER_AWE);
 
       
    //   HLAServer.addPCRISListener(new PlayerCasualtyHandler(log), HLAInteractionType.PLAYER_CASUALTY);
 
       
//       HLAServer.addPCRISListener(new PlayerCheatStatusHandler(log), HLAInteractionType.PLAYER_CHEAT_STATUS);
 
       
//       HLAServer.addPCRISListener(new PlayerHitHandler(log), HLAInteractionType.PLAYER_HIT);
 
       
    //   HLAServer.addPCRISListener(new PlayerInstrumentationStatusHandler(log), HLAInteractionType.PLAYER_INSTRUMENTATION_STATUS);
 
//       HLAServer.addPCRISListener(new PlayerStatusHandler2(log) ,HLAInteractionType.PLAYER_STATUS);
    //   
//       HLAServer.addPCRISListener(new PlayerNBCHandler(log), HLAInteractionType.PLAYER_NBC);
 
       
//       HLAServer.addPCRISListener(new PlayerWeaponFiredHandler(log), HLAInteractionType.PLAYER_WEAPON_FIRED);
 
       
//       HLAServer.addPCRISListener(new RequestDataHandler(this, log), HLAInteractionType.REQUEST_DATA);
 
       
       HLAServer.addPCRISListener(new RequestPlayerInitializationHandler(log), HLAInteractionType.REQUEST_PLAYER_INITIALIZATION);
 
       
       HLAServer.addPCRISListener(new SystemAlertHandler(log), HLAInteractionType.SIMULATION_ERROR);
 
       
//       HLAServer.addPCRISListener(new SmartTargetStatusHandler(log), HLAInteractionType.SMART_TARGET_STATUS);
 
       
//       HLAServer.addPCRISListener(new SystemAlertHandler(log), HLAInteractionType.SYSTEM_ALERT);
     
     }
     else {
 
       
       logger.severe("HLAServer is NOT ready, unable to register interaction listeners!");
     } 
   }

   public class HLASender
     implements HLAMessageSender
   {
     private Object lock = new Object();
  
     public void sendMessage(BaseInteraction param1BaseInteraction) {
       synchronized (this.lock) {
 
         
         try {
           HLAControl.this.outputQueue.offer(param1BaseInteraction);
         }
         catch (Exception exception) {
           
           HLAControl.logger.severe(exception.getMessage());
         } 
       } 
     }
   }


   private void checkOutputQueue() {
     BaseInteraction baseInteraction = null;
     
     try {
       baseInteraction = this.outputQueue.take();
       publishInteraction(baseInteraction);
     }
     catch (Exception exception) {
       
       logger.info(exception.toString());
     } 
   }


   private class OutputProcessor
     extends Thread
   {
     boolean alive;

  
   //  public void terminate() {
   //    this.alive = false;
   //  }
 
     public void run() {
       this.alive = true;
       while (this.alive)
       {
         
         HLAControl.this.checkOutputQueue();
       }
     }
   }
 }
