 package dcnclistener.hla;
 
  import com.cubic.pcris.BaseInteraction;
 import com.cubic.pcris.PlayerCasualtyInteraction;
 import com.cubic.util.LogFileWriter;

 public class PlayerCasualtyHandler
   extends ReceivedInteractionHandler
 {
   public PlayerCasualtyHandler(LogFileWriter paramLogFileWriter) {
     super(paramLogFileWriter);
   }

   public void receivedInteraction(BaseInteraction paramBaseInteraction) {
     synchronized (HLAControl.getInteractionLock()) {
       
       if (shouldProcess(paramBaseInteraction)) {
         
         logIt(paramBaseInteraction);
         handle((PlayerCasualtyInteraction)paramBaseInteraction);
         mark();
       } 
     } 
   }

 
   
   protected void handle(PlayerCasualtyInteraction paramPlayerCasualtyInteraction) {
//     try {
//       MsgLogger.getInstance().logUplinkMessage(MsgLogger.LogEventType.FROM_DCNC, 27, 
//           
//           (int)paramPlayerCasualtyInteraction.getPlayerId(), paramPlayerCasualtyInteraction.getPlayerUnitId(), paramPlayerCasualtyInteraction
//           .getBDAStatus(), paramPlayerCasualtyInteraction.getTime());
//       
//       List<ProtobufMessagePacket> list = TranslatorFOMToGPB.translatePlayerCasualtyInteraction(paramPlayerCasualtyInteraction);
//       
//       for (ProtobufMessagePacket protobufMessagePacket : list)
//       {
//         this.gpbSender.sendMessage(protobufMessagePacket);
//       }
//     }
//     catch (Exception exception) {
//       
//       logException("PlayerCasualtyHandler", exception);
//     } 
   }
 }
