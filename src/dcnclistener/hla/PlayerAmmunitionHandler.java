 package dcnclistener.hla;
 
  import com.cubic.pcris.BaseInteraction;
import com.cubic.pcris.PlayerAmmunitionInteraction;
import com.cubic.util.LogFileWriter;




 public class PlayerAmmunitionHandler
   extends ReceivedInteractionHandler
 {
   public PlayerAmmunitionHandler(LogFileWriter paramLogFileWriter) {
     super(paramLogFileWriter);
   }

 
 
   
   public void receivedInteraction(BaseInteraction paramBaseInteraction) {
     synchronized (HLAControl.getInteractionLock()) {
    	 System.out.println("New PlayerAmmunitionInteraction");
       if (shouldProcess(paramBaseInteraction)) {
         
         logIt(paramBaseInteraction);
         handle((PlayerAmmunitionInteraction)paramBaseInteraction);
         mark();
       } 
     } 
   }

 
 
 
   
   protected void handle(PlayerAmmunitionInteraction paramPlayerAmmunitionInteraction) {
	   System.out.println("pl");
//     try {
//       List<ProtobufMessagePacket> list = TranslatorFOMToGPB.translatePlayerAmmunitionInteraction(paramPlayerAmmunitionInteraction);
//       
//       for (ProtobufMessagePacket protobufMessagePacket : list)
//       {
//         this.gpbSender.sendMessage(protobufMessagePacket);
//       }
//     }
//     catch (Exception exception) {
//       
//       logException("PlayerAmmunitionHandler", exception);
//     } 
   }
 }
