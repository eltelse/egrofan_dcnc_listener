
 package dcnclistener.hla;
 
 import com.cubic.pcris.BaseInteraction;
import com.cubic.pcris.MATBATRangeConfirmationInformation;
import com.cubic.pcris.MATBATRangeConfirmationInteraction;
import com.cubic.util.LogFileWriter;






 public class MATBATRangeConfirmationHandler
   extends ReceivedInteractionHandler
 {
   public MATBATRangeConfirmationHandler(LogFileWriter paramLogFileWriter) {
     super(paramLogFileWriter);
   }

 
 
   
   public void receivedInteraction(BaseInteraction paramBaseInteraction) {
     synchronized (HLAControl.getInteractionLock()) {
       
       if (shouldProcess(paramBaseInteraction)) {
         
         logIt(paramBaseInteraction);
         handle((MATBATRangeConfirmationInteraction)paramBaseInteraction);
       } 
     } 
   }

   
   protected void handle(MATBATRangeConfirmationInteraction paramMATBATRangeConfirmationInteraction) {
     try {
    	 StringBuilder sb = new StringBuilder();
    	 sb.append("MATBATRangeConfirmationInteraction ");
    	 sb.append(paramMATBATRangeConfirmationInteraction.getMATBATResponses().length);
    	 MATBATRangeConfirmationInformation mk;
    	 for (int i = 0 ; i <paramMATBATRangeConfirmationInteraction.getMATBATResponses().length; i++){
    		 mk= paramMATBATRangeConfirmationInteraction.getMATBATResponses()[i];
    		 sb.append(" getEntityID  "+mk.getEntityID() +
    				 " getPlayerUnitId  "+mk.getPlayerUnitId() +
    				 " getCurrentLiveFireMode  "+mk.getCurrentLiveFireMode());
   	 }
       System.out.println(sb);
       mark();
     }
     catch (Exception exception) {
       
       this.log.log(exception);
     } 
   }
 }

