 package dcnclistener.hla;
 
  import com.cubic.pcris.BaseInteraction;
 import com.cubic.pcris.PlayerHitInteraction;
 import com.cubic.util.LogFileWriter;






public class PlayerHitHandler
   extends ReceivedInteractionHandler
 {
   public PlayerHitHandler(LogFileWriter paramLogFileWriter) {
     super(paramLogFileWriter);
   }

 
 
   
   public void receivedInteraction(BaseInteraction paramBaseInteraction) {
     synchronized (HLAControl.getInteractionLock()) {
       
       if (shouldProcess(paramBaseInteraction)) {
         
         logIt(paramBaseInteraction);
         handle((PlayerHitInteraction)paramBaseInteraction);
         mark();
       } 
     } 
   }

 
   
   protected void handle(PlayerHitInteraction paramPlayerHitInteraction) {
//     try {
//       MsgLogger.getInstance().logUplinkMessage(MsgLogger.LogEventType.FROM_DCNC, 29, 
//           
//           (int)paramPlayerHitInteraction.getPlayerId(), paramPlayerHitInteraction.getPlayerUnitId(), paramPlayerHitInteraction
//           .getBDAStatus(), paramPlayerHitInteraction.getTime());
//       
//       List<ProtobufMessagePacket> list = TranslatorFOMToGPB.translatePlayerHitInteraction(paramPlayerHitInteraction);
//       
//       for (ProtobufMessagePacket protobufMessagePacket : list)
//       {
//         this.gpbSender.sendMessage(protobufMessagePacket);
//       }
//     }
//     catch (Exception exception) {
//       
//       logException("PlayerHitHandler", exception);
//     } 
   }
 }

