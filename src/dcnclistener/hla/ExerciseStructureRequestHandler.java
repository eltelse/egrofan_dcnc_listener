 package dcnclistener.hla;
 
 import dcnclistener.exercise.ExerciseScenarioManager;

// import dcncgateway.translation.TranslatorGPBToFOM;
 import com.cubic.pcris.BaseInteraction;
import com.cubic.pcris.ExerciseStructureRequestInteraction;
import com.cubic.util.LogFileWriter;
public class ExerciseStructureRequestHandler
   extends ReceivedInteractionHandler
 {
   
   public ExerciseStructureRequestHandler(LogFileWriter paramLogFileWriter, ExerciseScenarioManager paramExerciseScenarioManager) {
     super(paramLogFileWriter);
   }

   public void receivedInteraction(BaseInteraction paramBaseInteraction) {
     synchronized (HLAControl.getInteractionLock()) {
       
       if (shouldProcess(paramBaseInteraction)) {
         
         logIt(paramBaseInteraction);
         handle((ExerciseStructureRequestInteraction)paramBaseInteraction);
       } 
     } 
   }

   protected void handle(ExerciseStructureRequestInteraction paramExerciseStructureRequestInteraction) {
     
     try {
       int i = (int)paramExerciseStructureRequestInteraction.getExerciseId();
       if (i > 0) {
         
       }
       else {
         
         logger.info("ExerciseStructure request for Exercise=" + i + " not handled.");
       } 
 
       
       mark();
     }
     catch (Exception exception) {
       
       this.log.log(exception);
     } 
   }
 }

