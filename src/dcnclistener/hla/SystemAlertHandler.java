 package dcnclistener.hla;
 
  import com.cubic.pcris.BaseInteraction;
 import com.cubic.pcris.SystemAlertInteraction;
 import com.cubic.util.LogFileWriter;




 
 public class SystemAlertHandler
   extends ReceivedInteractionHandler
 {
   public SystemAlertHandler(LogFileWriter paramLogFileWriter) {
     super(paramLogFileWriter);
   }
   
   public void receivedInteraction(BaseInteraction paramBaseInteraction) {
     synchronized (HLAControl.getInteractionLock()) {
       
       if (shouldProcess(paramBaseInteraction)) {
         
         logIt(paramBaseInteraction);
         handle((SystemAlertInteraction)paramBaseInteraction);
         mark();
       } 
     } 
   }

   
   protected void handle(SystemAlertInteraction paramSystemAlertInteraction) {
//     try {
//       List<ProtobufMessagePacket> list = TranslatorFOMToGPB.translate((BaseInteraction)paramSystemAlertInteraction);
//       
//       for (ProtobufMessagePacket protobufMessagePacket : list)
//       {
//         this.gpbSender.sendMessage(protobufMessagePacket);
//       }
//     }
//     catch (Exception exception) {
//       
//       logException("SystemAlertHandler", exception);
//     } 
   }
 }
