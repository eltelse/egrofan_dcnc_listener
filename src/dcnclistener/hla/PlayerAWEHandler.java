 package dcnclistener.hla;
 
  import com.cubic.pcris.BaseInteraction;
 import com.cubic.pcris.PlayerAWEInteraction;
 import com.cubic.util.LogFileWriter;

 
 public class PlayerAWEHandler
   extends ReceivedInteractionHandler
 {
   public PlayerAWEHandler(LogFileWriter paramLogFileWriter) {
     super(paramLogFileWriter);
   }
   
   public void receivedInteraction(BaseInteraction paramBaseInteraction) {
     synchronized (HLAControl.getInteractionLock()) {
       
       if (shouldProcess(paramBaseInteraction)) {
         
         logIt(paramBaseInteraction);
         handle((PlayerAWEInteraction)paramBaseInteraction);
         mark();
       } 
     } 
   }

   
   protected void handle(PlayerAWEInteraction paramPlayerAWEInteraction) {
//     try {
//       MsgLogger.getInstance().logUplinkMessage(MsgLogger.LogEventType.FROM_DCNC, 26, 
//           
//           (int)paramPlayerAWEInteraction.getPlayerId(), paramPlayerAWEInteraction.getPlayerUnitId(), paramPlayerAWEInteraction
//           .getBDAStatus(), paramPlayerAWEInteraction.getTime());
//       
//       List <ProtobufMessagePacket> list = TranslatorFOMToGPB.translatePlayerAWEInteraction(paramPlayerAWEInteraction);
//       
//       for (ProtobufMessagePacket protobufMessagePacket : list)
//       {
//         this.gpbSender.sendMessage(protobufMessagePacket);
//       }
//     }
//     catch (Exception exception) {
//       
//       logException("PlayerAWEHandler", exception);
//     } 
   }
 }

