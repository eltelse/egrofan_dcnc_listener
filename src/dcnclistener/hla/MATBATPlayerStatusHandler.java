 package dcnclistener.hla;
 
  import redis.clients.jedis.Jedis;



import com.cubic.pcris.BaseInteraction;
import com.cubic.pcris.LatLonAlt;
import com.cubic.pcris.MATBATPlayerStatusInteraction;
import com.cubic.util.LogFileWriter;

import dcnclistener.redis.RedisMsg;

 public class MATBATPlayerStatusHandler
   extends ReceivedInteractionHandler
 {
	Jedis jedis = new Jedis();
   public MATBATPlayerStatusHandler(LogFileWriter paramLogFileWriter) {
     super(paramLogFileWriter);

   }
   
   public void receivedInteraction(BaseInteraction paramBaseInteraction) {
     synchronized (HLAControl.getInteractionLock()) {
				System.out.println("New MATBATPlayerStatus");
       
       if (shouldProcess(paramBaseInteraction)) {
         
         logIt(paramBaseInteraction);
         handle((MATBATPlayerStatusInteraction)paramBaseInteraction);

         mark();
       } 
     } 
   }
   /** Zero degrees. */
   private static final int ZERO_DEGREES = 0;
   /** Ninety degrees. */
   private static final int NINETY_DEGREES = 90;
   /** 180 degrees. */
   private static final int ONEHUNDREDEIGHTY_DEGREES = 180;
   /** 270 degrees. */
   private static final int TWOHUNDREDSEVENTY_DEGREES = 270;
   /** 360 degrees. */
   private static final int THREEHUNDREDSIXTY_DEGREES = 360;

   /** Stationary velocity (0 mps). */
   private static final int STATIONARY_VELOCITY = 0;

   /** Millimeters per meter. */
   private static final int MILLIMETERS_PER_METER = 1000;
   
  
   
   public String getCorrectedLongitude(LatLonAlt data)
   {
	   return(Double.toString(  
			   (data.getLongitude() <= ONEHUNDREDEIGHTY_DEGREES) ?
		               data.getLongitude() :
		               ((THREEHUNDREDSIXTY_DEGREES - data.getLongitude()) )));
   }

   public String getCorrectedLatitude(LatLonAlt data)
   {
	   return(Double.toString(  
	           (data.getLatitude() >= NINETY_DEGREES) ?
	                   (data.getLatitude() - NINETY_DEGREES) :
	                   (Math.abs(NINETY_DEGREES - data.getLatitude()) * -1)));
   }

   


 int fer= 1;
 
 public String getTime(long time){
	 System.out.println(time+ " - Long.toString(time)"+ Long.toString(time) +
			"String.valueOf(time)-"+ String.valueOf(time) );
	 return Long.toString(time);
	 //String.valueOf(time);
 }
   boolean endi= false;
   protected void handle(MATBATPlayerStatusInteraction paramMATBATPlayerStatusInteraction) {
			// TODO Auto-generated method stub
	   if (endi)
		   return;

	fer++;
			RedisMsg redisMsg = new RedisMsg();
			//redisMsg.addValue("MATBATPlayerStatusInteraction", fer);
			LatLonAlt position = paramMATBATPlayerStatusInteraction.getPosition();
			redisMsg.addValue(" Longitude",getCorrectedLongitude(position));
			System.out.println("A - " +position.getLatitude());
			redisMsg.addValue(" Latitude",getCorrectedLatitude(position));
			System.out.println(" B - "+ getCorrectedLatitude(position));
			

	        StringBuffer buffer = new StringBuffer();
	        buffer.append(Double.toString(position.getLatitude()));
	        buffer.append(", ");
	        buffer.append(Double.toString(position.getLongitude()));
	        buffer.append(", ");
	        buffer.append(Double.toString(position.getAltitude()));

	        System.out.println(buffer);
			//redisMsg.addValue("Lat44",Double.toString(paramMATBATPlayerStatusInteraction.getPosition().getLatitude()));
		//	redisMsg.addValue("Lon",Double.toString(paramMATBATPlayerStatusInteraction.getPosition().getLongitude()));
							
			redisMsg.addValue(" PlayerUnitId",paramMATBATPlayerStatusInteraction.getPlayerUnitId());
			System.out.println(" PlayerUnitId - "+ paramMATBATPlayerStatusInteraction.getPlayerUnitId());
			redisMsg.addValue(" Time",getTime(paramMATBATPlayerStatusInteraction.getTime()));
			//redisMsg.addValue(" Time2",getTime(paramMATBATPlayerStatusInteraction.getPlayerId()));
			
			redisMsg.addValue(" Time",getTime(paramMATBATPlayerStatusInteraction.getTime()));
			
			
		//	redisMsg.addValue("PlayerId",paramMATBATPlayerStatusInteraction.getPlayerId());
			//paramMATBATPlayerStatusInteraction.gets
			jedis.publish("DcncInfo", redisMsg.serilize());
			//System.out.println(redisMsg.serilize());
			//System.out.println("end");
		//	endi = true;
		//	System.exit(0);
//     try {
//           (int)paramMATBATPlayerStatusInteraction.getPlayerId(), paramMATBATPlayerStatusInteraction.getPlayerUnitId(), paramMATBATPlayerStatusInteraction
//           .getBDAStatus(), paramMATBATPlayerStatusInteraction.getTime());
//       
//       List <ProtobufMessagePacket>list = TranslatorFOMToGPB.translateMATBATPlayerStatusInteraction(paramMATBATPlayerStatusInteraction);
//       
//       if (list != null)
//       {
//         for (ProtobufMessagePacket protobufMessagePacket : list)
//         {
//           this.gpbSender.sendMessage(protobufMessagePacket);
//         }
//       }
//     }
//     catch (Exception exception) {
//       
//       logException("MATBATPlayerStatusHandler", exception);
//     } 
   }
 }
