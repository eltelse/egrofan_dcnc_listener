 package dcnclistener.hla;
 
  import com.cubic.pcris.BaseInteraction;
import com.cubic.pcris.RequestPlayerInitializationInteraction;
import com.cubic.util.LogFileWriter;
import com.cubic.utils.ApplicationPreferences;
public class RequestPlayerInitializationHandler
   extends ReceivedInteractionHandler
 {
   boolean LOOP_BACK = ApplicationPreferences.getBoolean("requestPlayerInitLoopBack", 
       Boolean.valueOf(false));
 

   public RequestPlayerInitializationHandler(LogFileWriter paramLogFileWriter) {
     super(paramLogFileWriter);
   }
 
 
 

   public void receivedInteraction(BaseInteraction paramBaseInteraction) {
     synchronized (HLAControl.getInteractionLock()) {
       
       if (shouldProcess(paramBaseInteraction)) {
         
         logIt(paramBaseInteraction);
         RequestPlayerInitializationInteraction requestPlayerInitializationInteraction = (RequestPlayerInitializationInteraction)paramBaseInteraction;
         
         if (this.LOOP_BACK) {
        
           loopBack(requestPlayerInitializationInteraction);
         }
         else {
           
           handle(requestPlayerInitializationInteraction);
         } 
         mark();
       } 
     } 
   }

   private void handle(RequestPlayerInitializationInteraction paramRequestPlayerInitializationInteraction) {
     if (paramRequestPlayerInitializationInteraction.getRequestAll()) {
       
       logger.info("RequestPlayerInitializationHandler: ALL");
 
 
     
     }
     else {
 
 
       
       long[] arrayOfLong = paramRequestPlayerInitializationInteraction.getEntityId();
       
       for (byte b = 0; b < arrayOfLong.length; b++) {
 
         
//         PlayerInitializationInteraction playerInitializationInteraction = TranslatorGPBToFOM.getPlayerInitializationInteraction(l1, arrayOfLong[b]);
//         
//         if (playerInitializationInteraction != null) {
//           
//           logger.fine("Sending PlayerInitialization for entity=" + arrayOfLong[b]);
//           
//           HLAControl.getHLAMessageSender().sendMessage((BaseInteraction)playerInitializationInteraction);
// 
// 
// 
//         
//         }
//         else {
// 
// 
// 
// 
//           
////           List<ProtobufMessagePacket> list = TranslatorFOMToGPB.translate((BaseInteraction)paramRequestPlayerInitializationInteraction);
////           for (ProtobufMessagePacket protobufMessagePacket : list)
////           {
////             this.gpbSender.sendMessage(protobufMessagePacket);
////           }
//         } 
       } 
     } 
   }
 
 

   private void loopBack(RequestPlayerInitializationInteraction paramRequestPlayerInitializationInteraction) {
     long[] arrayOfLong = paramRequestPlayerInitializationInteraction.getEntityId();
     if (arrayOfLong != null)
     {
       for (byte b = 0; b < arrayOfLong.length; b++)
       {
         sendCannedPlayerInitForEntity(arrayOfLong[b]);
       }
     }
   }
 
 
 

   private void sendCannedPlayerInitForEntity(long paramLong) {
//     PlayerInitializationInteraction playerInitializationInteraction = TranslatorGPBToFOM.createCannedPlayerInit((int)paramLong);
//     
//     if (playerInitializationInteraction != null)
//     {
//       HLAControl.getHLAMessageSender().sendMessage((BaseInteraction)playerInitializationInteraction);
//     }
   }
 }
