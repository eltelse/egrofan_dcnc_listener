package dcnclistener.hla;

public final class HlaConstants {
  public static final String DCNC_FEDERATE_ID = "PCRIS-DCNC";
  
  public static final String ICS_FEDERATE_ID = "ICS";
  
  public static final String EXCON_FEDERATE_ID = "PCRIS-EXCON";
  
  public static final String UNKNOWN_FEDERATE_ID = "PCRIS-UNKNOWN";
  
  public static final boolean RECOVERY_DATA_DEFAULT = false;
  
  public static final long TIME_DEFAULT = 0L;
}

