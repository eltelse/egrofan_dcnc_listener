 package dcnclistener.hla;
 
  import com.cubic.pcris.BaseInteraction;
 import com.cubic.pcris.SimulationErrorInteraction;
 import com.cubic.util.LogFileWriter;
 
 public class SimulationErrorHandler
   extends ReceivedInteractionHandler
 {
   public SimulationErrorHandler(LogFileWriter paramLogFileWriter) {
     super(paramLogFileWriter);
   }

   public void receivedInteraction(BaseInteraction paramBaseInteraction) {
     synchronized (HLAControl.getInteractionLock()) {
       
       if (shouldProcess(paramBaseInteraction)) {
         
         logIt(paramBaseInteraction);
         handle((SimulationErrorInteraction)paramBaseInteraction);
         mark();
       } 
     } 
   }

 
 
   
   protected void handle(SimulationErrorInteraction paramSimulationErrorInteraction) {
//     try {
//       List<ProtobufMessagePacket> list = TranslatorFOMToGPB.translate((BaseInteraction)paramSimulationErrorInteraction);
//       
//       for (ProtobufMessagePacket protobufMessagePacket : list)
//       {
//         this.gpbSender.sendMessage(protobufMessagePacket);
//       }
//     }
//     catch (Exception exception) {
//       
//       logException("SimulationErrorHandler", exception);
//     } 
   }
 }
