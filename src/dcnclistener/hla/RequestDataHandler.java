 package dcnclistener.hla;
 
 import com.cubic.pcris.BaseInteraction;
import com.cubic.pcris.RequestDataInteraction;
import com.cubic.util.LogFileWriter;
 
 public class RequestDataHandler
   extends ReceivedInteractionHandler
 {
   
   public RequestDataHandler(HLAControl paramHLAControl, LogFileWriter paramLogFileWriter) {
     super(paramLogFileWriter);
   }
   public void receivedInteraction(BaseInteraction paramBaseInteraction) {
     if (shouldProcess(paramBaseInteraction)) {
       
       RequestDataInteraction requestDataInteraction = (RequestDataInteraction)paramBaseInteraction;
 
       
       if (requestDataInteraction.getTargetFederateID().equals("PCRIS-EXCON")) {
         
         logIt(paramBaseInteraction);
         
         switch (requestDataInteraction.getDataRequested().getValue()) {
 
           
           case 10:
//             GPBProcessor.getInstance().requestSecondaryExerciseData();
             break;
         } 
 
       
       } 
       mark();
     } 
   }
 }
