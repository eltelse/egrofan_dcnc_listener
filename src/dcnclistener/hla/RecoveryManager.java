 package dcnclistener.hla;
 
 import com.cubic.pcris.BaseInteraction;
 import com.cubic.pcris.RecoveryControlInteraction;
 import com.cubic.pcris.RequestRecoveryDataInteraction;
 import com.cubic.util.LogFileWriter;
 import com.cubic.util.ResourceManager;
 import com.cubic.util.ResourceNotFoundException;
 import com.cubic.xander.hla.HLAServer;
 import com.cubic.xander.hla.PCRISListener;
 import java.io.File;
 import java.io.FileInputStream;
 import java.io.FileNotFoundException;
 import java.io.FileOutputStream;
 import java.io.IOException;
 import java.io.ObjectInputStream;
 import java.io.ObjectOutputStream;
 import java.util.HashSet;
 import java.util.ResourceBundle;
 import java.util.Set;
 import java.util.TimerTask;

 public class RecoveryManager
   extends TimerTask
   implements PCRISListener
 {
   private static final ResourceBundle RES = ResourceBundle.getBundle("dcnclistener.hla.Res");
 
   private static long mark;
 
   
   private static Object lock;
 
   
   private static Set<String> responders;
 
   
   private static long expirationTime;
 
   
   private static long expirationDelay;
 
   
   private LogFileWriter log;
 
   
   private File recoveryFile;
 
 
   
   public RecoveryManager(LogFileWriter paramLogFileWriter) {
     this.log = paramLogFileWriter;
     if (lock == null) {
       
       mark = 0L;
       lock = new Object();
       responders = new HashSet<>();
       expirationTime = 0L;
 
       
       try {
         expirationDelay = ResourceManager.getResourceLong(RES
             .getString("ResourceID"), RES
             .getString("Res_RecoveryExpiration"));
         
         String str = ResourceManager.getResourceText(RES
             .getString("ResourceID"), RES
             .getString("Res_RecoveryFile"));
         paramLogFileWriter.log("RecoveryManager: recovery file name=" + str);
         this.recoveryFile = new File(str);
         if (!this.recoveryFile.exists()) {
           
           paramLogFileWriter.log("RecoveryManager: recovery file not found.");
           this.recoveryFile.createNewFile();
         } 
         FileInputStream fileInputStream = new FileInputStream(this.recoveryFile);
         ObjectInputStream objectInputStream = new ObjectInputStream(fileInputStream);
         
         mark = objectInputStream.readLong();
         
         objectInputStream.close();
         objectInputStream = null;
         fileInputStream.close();
         fileInputStream = null;
       }
       catch (ResourceNotFoundException resourceNotFoundException) {
         
         throw new InternalError(resourceNotFoundException.getMessage());
       }
       catch (FileNotFoundException fileNotFoundException) {
         
         paramLogFileWriter.log(fileNotFoundException);
         mark = System.currentTimeMillis();
       }
       catch (IOException iOException) {
         
         paramLogFileWriter.log(iOException);
         mark = System.currentTimeMillis();
       } 
     } 
   }

   public static void mark() {
     synchronized (lock) {
       
       mark = System.currentTimeMillis();
     } 
   }
 
 

   public RequestRecoveryDataInteraction getRecoveryRequest() {
     synchronized (lock) {
       
       expirationTime = System.currentTimeMillis() + expirationDelay;
       
       return new RequestRecoveryDataInteraction("", false, 
           System.currentTimeMillis(), mark);
     } 
   }


   public static boolean isCurrentRequest() {
     return (System.currentTimeMillis() < expirationTime || 
       !responders.isEmpty());
   }
 

   public void receivedInteraction(BaseInteraction paramBaseInteraction) {
     RecoveryControlInteraction recoveryControlInteraction = (RecoveryControlInteraction)paramBaseInteraction;
 
 
     
     if (recoveryControlInteraction.getRequestingFederateID().equals(HLAServer.getFederateName()))
     {
       if (recoveryControlInteraction.getReady() && System.currentTimeMillis() < expirationTime) {
 
 
 
         
         responders.add(recoveryControlInteraction.getOwningFederateId());
       }
       else if (!recoveryControlInteraction.getReady()) {
 
 
         
         responders.remove(recoveryControlInteraction.getOwningFederateId());
       } 
     }
   }
 

   public void run() {
     try {
       FileOutputStream fileOutputStream = new FileOutputStream(this.recoveryFile);
       ObjectOutputStream objectOutputStream = new ObjectOutputStream(fileOutputStream);
       
       objectOutputStream.writeLong(mark);
       
       objectOutputStream.flush();
       objectOutputStream.close();
       objectOutputStream = null;
       fileOutputStream.flush();
       fileOutputStream.close();
       fileOutputStream = null;
     }
     catch (FileNotFoundException fileNotFoundException) {
       
       this.log.log("Recovery file " + this.recoveryFile.getAbsolutePath());
       this.log.log(fileNotFoundException);
     }
     catch (IOException iOException) {
       
       this.log.log(iOException);
     } 
   }
 }
