 package dcnclistener.hla;
 
  import com.cubic.pcris.BaseInteraction;
 import com.cubic.pcris.PlayerWeaponFiredInteraction;
 import com.cubic.util.LogFileWriter;




 public class PlayerWeaponFiredHandler
   extends ReceivedInteractionHandler
 {
   public PlayerWeaponFiredHandler(LogFileWriter paramLogFileWriter) {
     super(paramLogFileWriter);
   }

 
 
   
   public void receivedInteraction(BaseInteraction paramBaseInteraction) {
     synchronized (HLAControl.getInteractionLock()) {
       
       if (shouldProcess(paramBaseInteraction)) {
         
         logIt(paramBaseInteraction);
         handle((PlayerWeaponFiredInteraction)paramBaseInteraction);
         mark();
       } 
     } 
   }

 
 
   
   protected void handle(PlayerWeaponFiredInteraction paramPlayerWeaponFiredInteraction) {
//     try {
//       List<ProtobufMessagePacket> list = TranslatorFOMToGPB.translatePlayerWeaponFiredInteraction(paramPlayerWeaponFiredInteraction);
//       
//       for (ProtobufMessagePacket protobufMessagePacket : list)
//       {
//         this.gpbSender.sendMessage(protobufMessagePacket);
//       }
//     }
//     catch (Exception exception) {
//       
//       logException("PlayerWeaponFireHandler", exception);
//     } 
   }
 }

