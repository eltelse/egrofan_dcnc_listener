package dcnclistener.hla;

import com.cubic.pcris.BaseInteraction;

public interface HLAMessageSender {
  void sendMessage(BaseInteraction paramBaseInteraction);
}
