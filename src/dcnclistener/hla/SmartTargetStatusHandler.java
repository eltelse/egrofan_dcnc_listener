 package dcnclistener.hla;
 
  import com.cubic.pcris.BaseInteraction;
 import com.cubic.pcris.SmartTargetStatusInteraction;
 import com.cubic.util.LogFileWriter;




 
 public class SmartTargetStatusHandler
   extends ReceivedInteractionHandler
 {
   public SmartTargetStatusHandler(LogFileWriter paramLogFileWriter) {
     super(paramLogFileWriter);
   }

 
 
   
   public void receivedInteraction(BaseInteraction paramBaseInteraction) {
     synchronized (HLAControl.getInteractionLock()) {
       
       if (shouldProcess(paramBaseInteraction)) {
         
         logIt(paramBaseInteraction);
         handle((SmartTargetStatusInteraction)paramBaseInteraction);
         mark();
       } 
     } 
   }

 
 
   
   protected void handle(SmartTargetStatusInteraction paramSmartTargetStatusInteraction) {
//     try {
//       List <ProtobufMessagePacket>list = TranslatorFOMToGPB.translate((BaseInteraction)paramSmartTargetStatusInteraction);
//       
//       for (ProtobufMessagePacket protobufMessagePacket : list)
//       {
//         this.gpbSender.sendMessage(protobufMessagePacket);
//       }
//     }
//     catch (Exception exception) {
//       
//       logException("SmartTargetStatusHandler", exception);
//     } 
   }
 }
