 package dcnclistener.hla;
 
 import dcnclistener.exercise.ExerciseScenarioManager;

import com.cubic.pcris.BaseInteraction;
import com.cubic.pcris.ExerciseListInteraction;
import com.cubic.pcris.ExerciseListRequestInteraction;
import com.cubic.util.LogFileWriter;

public class ExerciseListRequestHandler
   extends ReceivedInteractionHandler
 {
   private ExerciseScenarioManager exerciseManager;
   
   public ExerciseListRequestHandler(LogFileWriter paramLogFileWriter, ExerciseScenarioManager paramExerciseScenarioManager) {
     super(paramLogFileWriter);
     this.exerciseManager = paramExerciseScenarioManager;
   }
   
   public void receivedInteraction(BaseInteraction paramBaseInteraction) {
     logger.info("ExerciseListRequestInteraction Rx...");
     synchronized (HLAControl.getInteractionLock()) {
       
       if (shouldProcess(paramBaseInteraction)) {
         
         logIt(paramBaseInteraction);
         handle((ExerciseListRequestInteraction)paramBaseInteraction);
       } 
     } 
   }

 
   
   protected void handle(ExerciseListRequestInteraction paramExerciseListRequestInteraction) {
     logger.fine("ExerciseListRequestHandler.handle executing...");
     ExerciseListInteraction exerciseListInteraction = null;
     
     try {
       exerciseListInteraction = this.exerciseManager.getExerciseList();
       if (exerciseListInteraction != null) {
 
         
         exerciseListInteraction.setTime(System.currentTimeMillis());
         HLAControl.getHLAMessageSender().sendMessage((BaseInteraction)exerciseListInteraction);
       } 
       mark();
     }
     catch (Exception exception) {
       
       this.log.log(exception);
     } 
   }
 }
