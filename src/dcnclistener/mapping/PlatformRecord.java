 package dcnclistener.mapping;
 
 public class PlatformRecord
 {
   public int platformId;
   public int exconVulnerability;
   public int dcncVulnerability;
   public int reportingRate;
   public int reportOnDistance;
   
   public PlatformRecord() {}
   
   public PlatformRecord(int paramInt1, int paramInt2, int paramInt3, int paramInt4, int paramInt5) {
     this.platformId = paramInt1;
     this.exconVulnerability = paramInt2;
     this.dcncVulnerability = paramInt3;
     this.reportingRate = paramInt4;
     this.reportOnDistance = paramInt5;
   }
 }

