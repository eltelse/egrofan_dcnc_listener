 package dcnclistener.mapping;
 
 import java.util.HashMap;
import java.util.Vector;
import java.util.logging.Logger;

import com.cubic.utils.ApplicationPreferences;
import com.cubic.utils.csv.CsvFieldReader;
 
 
 
 public class PlatformMap
 {
 //  private static final char COMMENTCHAR = '#';
 //  private static final int MIN_FIELDS = 4;
   private static final boolean DUMP_MAPS = Boolean.valueOf(ApplicationPreferences.get("DumpMaps", "false"))
     .booleanValue();
 
 
   
   private static final String FILE_PLATFORM_ATTRIBUTES_MAP = ApplicationPreferences.get("PlatformAttributesMap", "data/PlatformAttributesMap.csv");
 
 
   
   private static boolean errorFound = false;
 
 
   
   private static StringBuffer errorMsg = new StringBuffer();
 
 
   
   private static Logger logger = Logger.getLogger(PlatformMap.class.getName());

   private static HashMap<Integer, PlatformRecord> platformAttributesMap = new HashMap<>();


   public static String getErrorMessage() {
     return errorMsg.toString();
   }
 

   public static void initialize() {
     loadMaps();
   }

   public static PlatformRecord getPlatformInfo(int paramInt) {
     PlatformRecord platformRecord = null;
     
     if (platformAttributesMap.containsKey(Integer.valueOf(paramInt))) {
       
       platformRecord = platformAttributesMap.get(Integer.valueOf(paramInt));
     }
     else {
       
       logger.warning("PlatformId=" + paramInt + " has no mapped vulnerability.");
     } 
 
     
     return platformRecord;
   }

   public static int getExconVulnerability(int paramInt) {
     int i = -1;
     
     if (platformAttributesMap.containsKey(Integer.valueOf(paramInt))) {
       
       i = ((PlatformRecord)platformAttributesMap.get(
           Integer.valueOf(paramInt))).exconVulnerability;
     }
     else {
       
       logger.warning("PlatformId=" + paramInt + " has no mapped vulnerability.");
     } 
 
     
     return i;
   }

   public static int getDcncVulnerability(int paramInt) {
     int i = -1;
     
     if (platformAttributesMap.containsKey(Integer.valueOf(paramInt))) {
       
       i = ((PlatformRecord)platformAttributesMap.get(
           Integer.valueOf(paramInt))).dcncVulnerability;
     }
     else {
       
       logger.warning("PlatformId=" + paramInt + " has no mapped vulnerability.");
     } 
 
     
     return i;
   }

   private static void loadMaps() {
     readFile(platformAttributesMap, FILE_PLATFORM_ATTRIBUTES_MAP);
   }
 

   public static void parse(String paramString, HashMap<Integer, PlatformRecord> paramHashMap, Vector<String> paramVector, int paramInt) {
     if (((String)paramVector.get(0)).length() > 0)
     {
       if (((String)paramVector.get(0)).charAt(0) != '#') {
         
         String str = "";
         int i = 0;
         int j = 0;
         int k = 0;
         int m = 0;
         int n = 0;
         
         byte b = 0;
         if (paramVector.size() < 4) {
           
           errorFound = true;
           errorMsg.append("PlatformAttributesMap.parse(): Too few values at Line: " + paramInt + "\nFile: " + paramString + "\nExpected: " + '\004' + " Got: " + paramVector
               
               .size());
         } else {
 
           
           try {
             
             str = CsvFieldReader.getString(paramVector, b++);
             i = CsvFieldReader.getInteger(paramVector, b++);
             j = CsvFieldReader.getInteger(paramVector, b++);
             k = CsvFieldReader.getInteger(paramVector, b++);
             m = CsvFieldReader.getInteger(paramVector, b++);
             n = CsvFieldReader.getInteger(paramVector, b);
           }
           catch (Exception exception) {
             
             errorFound = true;
             errorMsg.append("PlatformAttributesMap.parse(): Could not parse " + (String)paramVector
                 
                 .get(b) + "\nLine: " + paramInt + " Index: " + b + " File: " + paramString);
           } 
         } 
 
 
         
         if (!errorFound) {
           
           if (DUMP_MAPS)
           {
             logger.info("Mapping Platform=" + str + " with ID=" + i + " to EXCON_vuln_Id=" + j + ", DCNC_vuln_Id=" + k + ", Report_rate=" + m + ", Report_on_distance=" + n);
           }
 
        
           paramHashMap.put(Integer.valueOf(i), new PlatformRecord(i, j, k, m, n));
         } 
       } 
     }
   }

   private static void readFile(HashMap<Integer, PlatformRecord> paramHashMap, String paramString) {
     if (DUMP_MAPS)
     {
       logger.info("Loading Platform Attributes mappings from " + paramString);
     }
 
     
     //CsvFileReader csvFileReader = null;
 
     
//     try {
//       csvFileReader = new CsvFileReader(paramString);
//       
////      //   Vector<String> vector = csvFileReader.readFields();
////     //    byte b = 1;
////       //  while (vector != null)
////       //  {
////           
////         //  vector = sanitize(vector);
////           
////      //     parse(paramString, paramHashMap, vector, b);
////      //     vector = csvFileReader.readFields();
////       //    b++;
////    //     }
////       
////       } catch (IOException iOException) {
////         
////         errorFound = true;
////         errorMsg.append("IO error when reading mapping file: " + paramString);
////         logger.log(Level.SEVERE, "Caught Exception", iOException);
////       }
//     
//     } catch (FileNotFoundException fileNotFoundException) {
//       
//       errorFound = true;
//       errorMsg.append("Mapping file not found: " + paramString);
//       logger.log(Level.SEVERE, "Caught Exception", fileNotFoundException);
//     }
//     catch (Exception exception) {
//       
//       errorFound = true;
//       errorMsg.append("Error when reading mapping file: " + paramString);
//       logger.log(Level.SEVERE, "Caught Exception", exception);
//     } 
     
     if (errorMsg != null)
     {
       logger.info(errorMsg.toString());
     }
   }
 
 
 

//   private static Vector<String> sanitize(Vector<String> paramVector) {
//    // Vector<String> vector = new Vector(0);
//     
//     for (String str1 : paramVector) {
//       
//       String str2 = str1;
//       if (str1.length() > 0) {
//         
//         str2 = str2.trim();
// 
//         
//         str2 = str2.replaceAll("[\\n\\t ]", "");
//       } 
//       vector.add(str2);
//     } 
//     
//     return vector;
//   }
 

   public static boolean successfulParsing() {
     return !errorFound;
   }
 }


/*\mapping\PlatformMap.class

 */