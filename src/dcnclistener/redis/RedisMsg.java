package dcnclistener.redis;

import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

public class RedisMsg {
	private static String NEW_LINE_DASH = "\n";
	private static String MIDDLE_DASH = "\n";
	
	private Map<String, String> mMsgMap = new HashMap<String, String>();	
	
	public void addValue(String key, String val){
		put(key, val);		
	}
	
	public String serilize(){
		StringBuilder msg = new StringBuilder();
		for (Entry<String , String> msgMapEntry : mMsgMap.entrySet()){
			msg.append(msgMapEntry.getKey())
				.append(MIDDLE_DASH)
				.append(msgMapEntry.getValue())
				.append(NEW_LINE_DASH);
		}
		return msg.toString();
	}
	
	public void clear(){
		mMsgMap.clear();
	}
	
	public void put(String key, String val){
		mMsgMap.put(key, val);		
	}
	
	public Set<Entry<String, String>> entrySet(){
		return mMsgMap.entrySet();
	}

	public void addValue(String key, double value) {
		addValue(key, String.valueOf(value));
	}

	public void addValue(String key, int value) {
		addValue(key, String.valueOf(value));
	}
	
}
