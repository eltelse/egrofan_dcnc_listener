<!-- Sample configuration file for the HLA Server -->

<hla>

    <!-- HLAServer mode selection. "Pitch" or "Multicast". -->
    <mode>Multicast</mode>

    <!-- RTI configuration information. -->
    <rti>
        <!-- Host where the Primary CRC is waiting. -->
        <rti_host>Analyst2</rti_host>

        <!-- Host where the Secondary CRC is waiting. -->
        <secondary_host>Analyst2</secondary_host>

        <!-- Port the CRC is listening to. -->
        <crc_port>8989</crc_port>

        <!-- Name identifier for the federation. -->
        <federation_name>MATBAT-B</federation_name>

        <!-- Name identifier for the federate. -->
        <federate_name>DCNCListener</federate_name>

        <!-- Path to the federation file. -->
        <federation_file>data\MATBAT-FDD.xml</federation_file>

        <!-- Default interaction support which will be loaded when the
             HLA Server is initialized. Note that the HLA Server will
             dynamically notify the RTI of additional interactions any time
             a client tries to send one, or registers a listener for one not
             included here. Putting them here just speeds things up a bit
             as it allows the HLA Server to get them all out of the way
             ahead of time.
        -->
        <default_support>
            <!-- Interactions we will publish. -->
            <publish>HandshakeInteraction</publish>
            <publish>RequestRecoveryDataInteraction</publish>

            <!-- Interactions we will subscribe to. -->
            <subscribe>HandshakeInteraction</subscribe>
            <subscribe>RequestRecoveryDataInteraction</subscribe>

        </default_support>

    </rti>

    <!-- Local server configuration details. -->
    <server>
        <!-- Retry delay, in milliseconds. -->
        <retry_delay>1000</retry_delay>

        <!-- Maximum number of retry attempts. -->
        <retry_attempts>10</retry_attempts>

        <!-- Whether the processing queues should be persisted. -->
        <persist-queues>false</persist-queues>

        <!-- Database configuration. -->
        <database>
            <!-- ID of the outbound database. -->
            <outbound>HLA_Outbound</outbound>
            <!-- Cache period (in milliseconds) of the outbound database.
                 A longer this value allows for more recovery data to remain
                 available.
                 A shorter value will save on disk resources and improve startup
                 speeds.
            -->
            <cache-period>60000</cache-period>
        </database>

        <!-- Queue configuration. -->
        <queue>
            <inbound>
                <!-- ID of the inbound queue. -->
                <id>HLA_Inbound</id>

                <!-- # of processing daemons on the inbound queue. -->
                <daemons>1</daemons>

            </inbound>

            <outbound>
                <!-- ID of the outbound queue. -->
                <id>HLA_Outbound</id>

                <!-- # of processing daemons on the outbound queue. -->
                <daemons>1</daemons>
            </outbound>
        </queue>

        <!-- Handshake configuration -->
        <handshake>
            <!-- Periodic rate at which the Handshake should be sent, in
                 milliseconds. All Federates should Handshake at the same rate,
                 so this is also the rate at which the Federate will expect
                 to receive Handshakes.
            -->
            <period>60000</period>

            <!-- Milliseconds which can ellapse with a Handshake being received,
                 after which we assume the Federate has gone dark.
            -->
            <outage-period>120000</outage-period>
        </handshake>

        <monitor>
            <!-- Whether or not to enable the CRC monitor. -->
            <enable>false</enable>

            <!-- Periodic rate at which the CRC verifies the existence of the
                 CRC.
            -->
            <monitor-period>5000</monitor-period>
        </monitor>

    </server>

    <dcnclistener>
        <!-- HLA configuration specific to the DCNC Listener. -->
        <excon-federate-id>PCRIS-EXCON</excon-federate-id>

        <recovery>
            <!-- Periodic rate at which the recovery time is stored. -->
            <recovery-period>5000</recovery-period>
            <!-- Path to the recovery file. -->
            <recovery-file>data/RecoveryTimestamp.dat</recovery-file>
            <!-- Number of milliseconds to wait for a recovery request to
                 expire - responses sent after this time would be ignored.
            -->
            <expiration-period>5000</expiration-period>
        </recovery>
    </dcnclistener>

    <!-- Configuration of the LRC settings. These values are copied into the
         pRTI1516 LRC settings file.
    -->
    <lrc>
        <adapter>All</adapter>
        <time-factory-class>se.pitch.prti1516.LogicalTimeFactoryDouble</time-factory-class>
        <interval-factory-class>se.pitch.prti1516.LogicalTimeIntervalFactoryDouble</interval-factory-class>
        <save-path>c\:\\savedfed</save-path>
        <process-model>Multi-threaded</process-model>
        <nodns>false</nodns>
        <use-nio>true</use-nio>

        <udp>
            <overflow-strategy>Drop</overflow-strategy>
            <interval-micros>100000</interval-micros>
            <bit-rate>0</bit-rate>
            <smooth-buffer-size>0</smooth-buffer-size>
            <enable-bundling>true</enable-bundling>
            <bundle-size-limit>63000</bundle-size-limit>
            <drop-unprocessed-messages>false</drop-unprocessed-messages>
            <limit-rate>false</limit-rate>
            <bundle-age-limit>10</bundle-age-limit>
            <buffer-size>64000</buffer-size>
            <packet-rate>0</packet-rate>
            <smoothing>false</smoothing>
            <drop-message-age>1000</drop-message-age>

            <port-range>
                <start>5000</start>
                <end>5999</end>
                <allow-fallback>false</allow-fallback>
            </port-range>
        </udp>

        <booster>
            <enable>false</enable>
            <address></address>
            <pull>false</pull>
            <port>80</port>
        </booster>

        <tcp>
            <enable-bundling>false</enable-bundling>
            <limit-rate>false</limit-rate>
            <bundle-age-limit>0</bundle-age-limit>
            <packet-rate>0</packet-rate>
            <buffer-size>64000</buffer-size>
            <bundle-size-limit>0</bundle-size-limit>
            <bit-rate>0</bit-rate>
            <smoothing>false</smoothing>
            <overflow-strategy>Block</overflow-strategy>
            <interval-micros>10000</interval-micros>
            <smooth-buffer-size>0</smooth-buffer-size>

            <port-range>
                <start>6000</start>
                <end>6999</end>
                <allow-fallback>false</allow-fallback>
            </port-range>

            <advertise>
                <address></address>
                <mode>IP</mode>
            </advertise>
        </tcp>

        <product>
                <type>pRTI 1516</type>
                <version>v3.0</version>
        </product>
    </lrc>

	<!-- Configuration of the multicast transmission options. -->
    <multicast>
    	<server>
    		<!-- The interaction server port. This the the TCP port where this
    		     Federate will listen for connections from other Federates.
    		     This must, of course, be unique for every Federate running
    		     on the same interface.
    		 -->
			<port>42429</port>
                        <!--port 42426</port>-->
    	</server>

		<!-- Network interface to bind to. -->
		<net-if>lo</net-if>

    	<discovery>
	    	<!-- Address that the multicast packets are broadcast to. -->
			<group>
				<ip-address>224.42.1.1</ip-address>
				<port>42424</port>
			</group>
	
			<!-- Time-to-live (TTL) for the multicast datagram.
	
	             The TTL in IPv4 multicasting can also be thought of as a routing
				 threshold - it limits how long multicast traffic will expand
				 across routers. The TTL is decremented by 1 each time a datagram
				 passes across a router. Routers have a TTL threshold assigned to
				 each of their interfaces, and only datagrams with a TTL greater
				 than the interface's threshold are forwarded.
	
				 The following provides some guidance on TTL thresholds and their
				 associated scope:
	
				 TTL 		Scope
				 0 			Restricted to the same host. Won't be output by any
							interface.
				 1 			Restricted to the same subnet. Won't be forwarded by
							a router.
				 < 32		Restricted to the same site, organization or department.
				 < 64		Restricted to the same region.
				 < 128	Restricted to the same continent.
				 < 255	Unrestricted in scope. Global.
			-->
			<ttl>1</ttl>

			<!-- How often discovery packets are sent. -->
			<period>2000</period>
    	</discovery>
    
    </multicast>
</hla>
